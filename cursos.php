<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ingreso al Sistema</title>

<script language="javascript1.5">
function buscaPrece(dni)
{
  window.open('buscaprecedelcurso.php?dni='+dni,'opciones2')
}
function limpiar(frm)
{
document.frmCur.anio.value=""
document.frmCur.division.value=""
document.frmCur.idcurso.value=""
document.frmCur.ciclo.value=""
document.frmCur.especialidad.value=""
document.frmCur.resolucion.value=""
document.frmCur.turno.selectedIndex=0

}
function enviar(frm)
{
  if(confirm("Desea grabar el curso?"))
  {
     frm.submit()
  }
}
function elimina(curso,division,especial, id)
{
  if(confirm("Desea eliminar el curso \n"+curso+"� "+ division +"� "+ especial))
  {
     window.open("cursos.php?idcur="+id+"&elimina=S","_self"); 
  }
}
</script>
<body>
<?php
session_start();
include('conexion.php');
if($_GET["elimina"]=="S")
{
$cons="delete from ctpoba.cursos where idcurso='".$_GET["idcur"]."'";
$resalu=mysqli_query($link,$cons);
}

if($_POST["anio"]<>"")
{
   if($_POST["idcurso"]=="")
      {
          $cons="insert into ctpoba.cursos(anio,division, idciclo, idespecialidad, turno,resolucion, idpreceptor) values('".$_POST["anio"]."','".$_POST["division"]."','".$_POST["idciclo"]."','".$_POST["idespecialidad"]."','".$_POST["turno"]."','".$_POST["resolucion"]."','".$_POST["idpreceptor"]."')";
          $resalu=mysqli_query($link,$cons) or die("Error al agregar el curso.<hr>".mysqli_error($link)."<hr>".$cons);
      }
   else
      {
          $cons="update ctpoba.cursos set anio='".$_POST["anio"]."' ,division ='".$_POST["division"]."', idciclo='".$_POST["idciclo"]."',idespecialidad='".$_POST["idespecialidad"]."', turno='".$_POST["turno"]."' ,resolucion='".$_POST["resolucion"]."', idpreceptor='".$_POST['idpreceptor']."' where idcurso='".$_POST["idcurso"]."'";
          $resalu=mysqli_query($link,$cons)or die("Error al actualizar el curso.<hr>".mysqli_error($link)."<hr>".$cons);
	  }
}
$cons="select * from ctpoba.cursos where idcurso='".$_GET["idcur"]."'";
$resalu=mysqli_query($link,$cons);
$curso=mysqli_fetch_array($resalu)


?>
<form method="post" name="frmCur"  action="cursos.php">
<input type="hidden" name="idcurso" value="<?php print $curso["idcurso"]?>" />
<table align="center">
  <caption> Definicion de Cursos</caption>
  <tr><td>A&ntilde;o</td>
    <td><input type="text" size="4" maxlength="4"  name="anio" value="<?php print $curso["anio"]?>" /></td>
  </tr>
  <tr><td>Division</td><td><input type="text" size="20" maxlength="20"  name="division" value="<?php print $curso["division"]?>"  />
    </td></tr>
  <tr><td>Turno</td><td>
   <select name="turno">
      <option value="">-</option>
      <option value="M" <?php if($curso["turno"]=='M'){print " selected ";}?> >Ma&ntilde;ana</option>
      <option value="T" <?php if($curso["turno"]=='T'){print " selected ";}?> >Tarde</option>
      <option value="V" <?php if($curso["turno"]=='V'){print " selected ";}?> >Vespertino</option>
   </select>
   <select name='tipo'>
      <option value='0'>...</option>
      <option value='TA'> Aula</option>
      <option value='TC'> Contraturno</option>
      <option value='TT'> Taller</option>
      <option value='TP'>Proyecto Tecnol&oacute;gico</option>
      <option value='TE'>Educaci&oacute;n F&iacute;sica</option>
   </select>
   </td></tr>
  <tr><td>Ciclo</td>
      <td>
      <select name="idciclo">
			<option>...</option>
		<?php 
		  $cons="Select * from ctpoba.ciclos";
		  $res=mysqli_query($link,$cons) or die("Error al obtener listado de Ciclos<hr>".mysqli_error());
		  while($fila=mysqli_fetch_array($res)) 
		  {
		  	 print "<option value='".$fila["idciclo"]."' ";
		  	 if($curso["idciclo"]==$fila["idciclo"]){print " selected ";}
		  	 print ">".$fila["nombre"]."</option>";
		  	}
		?>      
      </select>
      </td></tr>
  <tr><td>Especialidad</td>
      <td>
      <select name="idespecialidad">
			<option value=0>...</option>
		<?php 
		  $cons="Select * from ctpoba.especialidades";
		  $res=mysqli_query($link,$cons) or die("Error al obtener listado de especialidades<hr>".mysqli_error());
		  while($fila=mysqli_fetch_array($res)) 
		  {
		  	 print "<option value='".$fila["idespecialidad"]."' ";
		  	 if($curso["idespecialidad"]==$fila["idespecialidad"]){print " selected ";}
		  	 print ">".$fila["nombre"]."</option>";
		  	}
		?>      
      </select>
      </td></tr>
  <tr><td>Resolucion</td><td><input type="text" name="resolucion"  value="<?php print $curso["resolucion"]?>" size="50" /></td></tr>
  
  <tr><td>Dni Preceptor</td><td>
       <input type="text" name="dnipreceptor"  value="<?php print $curso["dnipreceptor"]?>" size="7"  onblur='buscaPrece(this.value)'/>
       <input type="hidden" name="idpreceptor" id="idpreceptor"  value="<?php print $curso["idpreceptor"]?>" size="7"/> <label id='nombrePrece'></label></td></tr>
  <tr><td colspan="2">
  <img src="imagenes/folder_add_32.png" title="Limpiar formulario" onClick="javascript:limpiar(frmCur);" style="cursor:pointer"/>
  <img src="imagenes/save_32.png" width="32" title="Guardar cambios" height="32" onClick="javascript:enviar(frmCur);" style="cursor:pointer"/>
  </td>
</table>
</form>

<?php
$link=mysqli_connect("localhost","user","Pass.2018");
$cons="select idcurso,anio, division,c.codigo as nombreciclo, b.nombre as nombreespecialidad, a.idciclo, a.idespecialidad from ctpoba.cursos a left join ctpoba.especialidades b on a.idespecialidad=b.idespecialidad left join ctpoba.ciclos c on a.idciclo=c.idciclo order by a.idciclo, anio, division";
// aca vemos si se esta grabando o eliminando


$resalu=mysqli_query($link,$cons) or die("Error al obtener listado de cursos definidos<hr>".mysqli_error()."<hr>".$cons);
?>
  <table class="font9" align="center">
    <caption>Listado de Cursos Definidos</caption>
	<?php
	while($dato=mysqli_fetch_array($resalu))
	{	  ?>
	  <tr ><td>
	   <img src="imagenes/pencil_32.png" height="16" title="Modificar" style="cursor:pointer" onClick="javascript:window.open('cursos.php?idcur=<?php print $dato["idcurso"]?>','_self');">
	   <img src="imagenes/newspaper_search_32.png" height="16" style="cursor:pointer" title=" Ver asignaturas del curso" onClick="javascript:window.open('cursoasignaturas.php?curso=<?php print $dato["idcurso"]?>','_self');">
	   <img src="imagenes/close_16.png" width="16" height="16" style="cursor:pointer" title="Eliminar" onClick="javascript:elimina('<?php echo $dato["anio"]?>','<?php echo $dato["division"]?>','<?php echo $dato["especialidad"]?>','<?php echo $dato["idcurso"]?>');">
	   <img src="imagenes/page_text_warning_32.png" width="16" height="16" style="cursor:pointer" title="Listado de alumnos" onClick="javascript:window.open('vercurso.php?idcurso=<?php print $dato["idcurso"]?>','_self');">
	   
	   </td>
	  <td> <?php echo $dato["anio"]?></td><td><?php echo $dato["division"]?></td><td><?php echo "(".$dato["nombreciclo"].") ". $dato["nombreespecialidad"]?></td></tr>
	<?php }	?>

</table>

<iframe name='opciones2' style='padding-top:40px;border:2px solid black;display:block'></iframe>
</body>

</html>
