<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;  charset=utf-8" />
<meta charset="utf-8"> 
<style type="text/css">
div{
-moz-border-radius:10px;
-webkit-border-radius:10px;
-ms-border-radius:10px;
}
body  {
	text-align:center;
  	margin:0 auto;
	background:"imagenes/fondo.png";
	font-size:20px;
	}
.tachado {
font-family : Verdana,Arial;
font-size : 7.5pt;
font-style : normal;
line-height : normal;
text-decoration: line-through;
}	
</style>	
<title>Declaracion Jurada de Cargos</title>
</head>
<script>
function imprimir()
{
  document.getElementById('boton_prt').style.visibility='hidden'
  window.print();
 }
</script>
<?php
include('conexion.php');
function nombredefuncion($funci)
{
 include('conexion.php');
   $cons4="select * from ctpoba.funciones where idFuncion='".$funci."'";
   $resfuncion=mysqli_query($link,$cons4) or die(mysqli_error($link).$cons4);
   $datosfuncion=mysqli_fetch_array($resfuncion);
   return $datosfuncion["Descripcion"];
}
function nombredependencia($funci)
{
include('conexion.php');
   $cons4="select * from ctpoba.dependencias where cod_dep='".$funci."'";
   $resfuncion=mysqli_query($link,$cons4) or die(mysqli_error($link).$cons4);
   $datosfuncion=mysqli_fetch_array($resfuncion);
   return $datosfuncion["nom_dep"];
}
function traeciudad($ciu, $prov)
{
include('conexion.php');
$ciudadsql="select * from ctpoba.lista_ciudades where id=".$ciu." and relacion=".$prov;
$resciudad=mysqli_query($link,$ciudadsql) or die(mysqli_error($link));
$datociudad=mysqli_fetch_array($resciudad) or die (mysqli_error($link)); 
return $datociudad["opcion"];
}
$cons3="select * from ctpoba.agentes where ndoc=".$_GET["idagente"];
$resagente=mysqli_query($link,$cons3) or die(mysqli_error($link));
$datosAgente=mysqli_fetch_array($resagente);

$cons3="select * from ctpoba.cargosxagente where dni=".$_GET["idagente"]." order by ordenimpres";
$resagente=mysqli_query($link,$cons3) or die(mysqli_error($link));


?>
<body style="margin:0;padding:0">
<div  style="position:absolute; left:350px"><img id='boton_prt' src="imagenes/print3.jpg" style="cursor:pointer" title='Imprimir' onclick='imprimir()'></div>
<div id="encab_escudo" style="position:absolute; align:center; top:10px; left:10px; width: 286px; height: 252px;"> 
  <img src="imagenes/escudo.jpg" /> 
  <p><font size="3" face="Kunstler Script" > Provincia de Tierra del Fuego, Antartida<br>
    e Islas del Atlantico Sur<br>
    Republica Argentina</font></p>
 <p style="font-size:12px">Coordinaci&oacute;n Provincial de Recursos Humanos</p>
</div>
<div style="position:absolute;top:210px;left:2px;z-index:-9;"> <img src="imagenes/declajur1.jpg" /></div>
<div id='apeynom' style='align:left;position:absolute; left: 332px; top: 345px; width: 723px; height: 19px;'><?php echo $datosAgente["apeynom"]?></div>
<div id='nacimiento' style='align:left;position:absolute; left: 323px; top: 390px; width: 177px; height: 19px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php echo date('d-m-Y',strtotime($datosAgente["fnac"]))?></div>
<div id='ndoc' style='align:left;position:absolute; left: 777px; top: 390px; width: 177px; height: 19px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php echo $datosAgente["ndoc"]?></div>
<div id='domicilio' style='align:left;position:absolute; left: 226px; top: 420px; width: 522px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php echo $datosAgente["domcalle"]?> Nro <?php echo $datosAgente["domnro"]?> <?php echo $datosAgente["dompiso"]?> <?php echo $datosAgente["domdepto"]?></div>
<div id='localidad' style='align:left;position:absolute; left: 859px; top: 420px; width: 203px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php echo traeciudad($datosAgente["domciudad"],$datosAgente["domprovincia"])?></div>
<div id='tdocLC' style='align:left;position:absolute; left: 505px; top: 390px; width: 54px; height: 19px;'>------</div>
<div id='tdocLE' style='align:left;position:absolute; left: 562px; top: 390px; width: 54px; height: 19px;'>------</div>
<div id='tdocCI' style='align:left;position:absolute; left: 618px; top: 390px; width: 53px; height: 19px;'>------</div>
<div id='tdocDNI' style='align:left;position:absolute; left: 683px; top: 390px; width: 49px; height: 19px;'>-------</div>
<script>
 if('<?php echo strtoupper($datosAgente["tdoc"])?>'=='LC') {  document.getElementById('tdocLC').style.visibility='hidden'}
 if('<?php echo strtoupper($datosAgente["tdoc"])?>'=='LE') {  document.getElementById('tdocLE').style.visibility='hidden'}
 if('<?php echo strtoupper($datosAgente["tdoc"])?>'=='CI') {  document.getElementById('tdocCI').style.visibility='hidden'}
 if('<?php echo strtoupper($datosAgente["tdoc"])?>'=='DNI') {  document.getElementById('tdocDNI').style.visibility='hidden'}
</script>
<?php
//cargamos hasta 4 cargos
 $datosCargo1=mysqli_fetch_array($resagente);
 $datosCargo2=mysqli_fetch_array($resagente);
 $datosCargo3=mysqli_fetch_array($resagente);
 $datosCargo4=mysqli_fetch_array($resagente);
 $datosCargo5=mysqli_fetch_array($resagente);
 
 
 ?>
<!-- Cargo 1-->

<div style='align:left;position:absolute; left: 411px; top: 545px; width: 657px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php echo nombredependencia($datosCargo1["dependencia"])?></div>
<div style='align:left;position:absolute; left: 293px; top: 580px; width: 184px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php echo date('d-m-Y',strtotime($datosCargo1["altacargo"]))?></div>
<div style='align:left;position:absolute; left: 100px; top: 612px; width: 636px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php 
  if($datosCargo1["curso"]==0)
     {print nombredefuncion($datosCargo1["funcion"]);} 
   else 
     {print nombredefuncion($datosCargo1["funcion"])."(".$datosCargo1["horas"]." Hs. Cat)";}?>
	 
</div>
<!-- Situacion de Revista -->
<div id='tit1' style='align:left;position:absolute; left: 1038px;top: 624px; width: 24px; height: 21px;'>X</div>
<div id='int1' style='align:left;position:absolute; left: 181px; top: 656px; width: 24px; height: 21px;'>X</div>
<div id='sup1' style='align:left;position:absolute; left: 340px; top: 656px; width: 24px; height: 21px;'>X</div>
<div id='tra1' style='align:left;position:absolute; left: 761px; top: 656px; width: 24px; height: 21px;'>X</div>
<script>
 if('<?php echo $datosCargo1["sitrev"]?>'=='Ti')
  {
     document.getElementById('int1').style.visibility='hidden'
     document.getElementById('sup1').style.visibility='hidden'
     document.getElementById('tra1').style.visibility='hidden'
  }
 if('<?php echo $datosCargo1["sitrev"]?>'=='In')
  {
     document.getElementById('tit1').style.visibility='hidden'
     document.getElementById('sup1').style.visibility='hidden'
     document.getElementById('tra1').style.visibility='hidden'
  }
 if('<?php echo $datosCargo1["sitrev"]?>'=='Su')
  {
     document.getElementById('tit1').style.visibility='hidden'
     document.getElementById('int1').style.visibility='hidden'
     document.getElementById('tra1').style.visibility='hidden'
  }
 if('<?php echo $datosCargo1["sitrev"]?>'=='Tr')
  {
     document.getElementById('tit1').style.visibility='hidden'
     document.getElementById('int1').style.visibility='hidden'
     document.getElementById('sup1').style.visibility='hidden'
  }
 if('<?php echo $datosCargo1["sitrev"]?>'=='-')
  {
     document.getElementById('tit1').style.visibility='hidden'
     document.getElementById('int1').style.visibility='hidden'
     document.getElementById('sup1').style.visibility='hidden'
     document.getElementById('tra1').style.visibility='hidden'     
  }
</script>
<div style='align:left;position:absolute; left: 90px; top: 795px; width: 540px; height: 60px;font-size:12px'><?php print $datosCargo1["obs"]."-". $datosCargo1["licencia"]?></div>
<div style='align:left;position:absolute; left: 98px; top: 681px; width: 458px; height: 21px;'>
<?php 
    if($datosCargo1["horas"]<>0)
	{print $datosCargo1["CodCargo"]."-".$datosCargo1["espcurr"]."(".$datosCargo1["curso"]."&deg; ".$datosCargo1["division"]."&deg;)";}
?>
</div>

<!-- Cargo 2 -->
<div style='align:left;position:absolute; left: 404px; top: 872px; width: 657px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php echo nombredependencia($datosCargo2["dependencia"])?></div>
<div style='align:left;position:absolute; left: 293px; top: 904px; width: 184px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php echo date('d-m-Y',strtotime($datosCargo2["altacargo"]))?></div>
<div style='align:left;position:absolute; left: 99px; top: 936px; width: 636px; height: 21px; opacity:70; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php 
  if($datosCargo2["curso"]==0)
     {print nombredefuncion($datosCargo2["funcion"]);} 
   else 
     {print nombredefuncion($datosCargo2["funcion"])."(".$datosCargo2["horas"]." Hs. Cat)";}?>
</div>
<!-- Situacion de Revista -->
<div id='tit2' style='align:left;position:absolute; left: 1031px; top: 947px; width: 24px; height: 21px;'>X</div>
<div id='int2' style='align:left;position:absolute; left: 175px; top: 983px; width: 24px; height: 21px;'>X</div>
<div id='sup2' style='align:left;position:absolute; left: 334px; top: 983px; width: 24px; height: 21px;'>X</div>
<div id='tra2' style='align:left;position:absolute; left: 752px; top: 983px; width: 24px; height: 21px;'>X</div>
<script>
 if('<?php echo $datosCargo2["sitrev"]?>'=='Ti')
  {
     document.getElementById('int2').style.visibility='hidden'
     document.getElementById('sup2').style.visibility='hidden'
     document.getElementById('tra2').style.visibility='hidden'
  }
 if('<?php echo $datosCargo2["sitrev"]?>'=='In')
  {
     document.getElementById('tit2').style.visibility='hidden'
     document.getElementById('sup2').style.visibility='hidden'
     document.getElementById('tra2').style.visibility='hidden'
  }
 if('<?php echo $datosCargo2["sitrev"]?>'=='Su')
  {
     document.getElementById('tit2').style.visibility='hidden'
     document.getElementById('int2').style.visibility='hidden'
     document.getElementById('tra2').style.visibility='hidden'
  }
 if('<?php echo $datosCargo2["sitrev"]?>'=='Tr')
  {
     document.getElementById('tit2').style.visibility='hidden'
     document.getElementById('int2').style.visibility='hidden'
     document.getElementById('Sup2').style.visibility='hidden'
  }
 if('<?php echo $datosCargo2["sitrev"]?>'=='-')
  {
     document.getElementById('tit2').style.visibility='hidden'
     document.getElementById('int2').style.visibility='hidden'
     document.getElementById('sup2').style.visibility='hidden'
     document.getElementById('tra2').style.visibility='hidden'     
  }
  
</script>
<div style='align:left;position:absolute; left: 84px; top: 1116px; width: 540px; height: 60px;;font-size:12px'><?php print $datosCargo2["obs"]."-". $datosCargo2["licencia"]?></div>
<div style='align:left;position:absolute; left: 92px; top: 1005px; width: 458px; height: 21px;'>
<?php 
    if($datosCargo2["horas"]<>0)
	{print $datosCargo2["CodCargo"]."-".$datosCargo2["espcurr"]."(".$datosCargo2["curso"]."&deg;  ".$datosCargo2["division"]."&deg; )";	 }
?>
</div>

<!-- Cargo 3 -->





<div style='align:left;position:absolute; left: 410px; top: 1197px; width: 657px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php echo nombredependencia($datosCargo3["dependencia"])?></div>
<div style='align:left;position:absolute; left: 295px; top: 1230px; width: 184px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php echo date('d-m-Y',strtotime($datosCargo3["altacargo"]))?></div>
<div style='align:left;position:absolute; left: 104px; top: 1267px; width: 636px; height: 21px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php 
  if($datosCargo3["curso"]==0)
     {print nombredefuncion($datosCargo3["funcion"]);} 
   else 
     {print nombredefuncion($datosCargo3["funcion"])."(".$datosCargo3["horas"]." Hs. Cat)";}?>
</div>
<!-- Situacion de Revista -->
<div id='tit3' style='align:left;position:absolute; left: 1038px;top: 1271px; width: 24px; height: 21px;'>X</div>
<div id='int3' style='align:left;position:absolute; left: 182px; top: 1308px; width: 24px; height: 21px;'>X</div>
<div id='sup3' style='align:left;position:absolute; left: 340px; top: 1309px; width: 24px; height: 21px;'>X</div>
<div id='tra3' style='align:left;position:absolute; left: 759px; top: 1306px; width: 24px; height: 21px;'>X</div>
<script>
 if('<?php echo $datosCargo3["sitrev"]?>'=='Ti')
  {
     document.getElementById('int3').style.visibility='hidden'
     document.getElementById('sup3').style.visibility='hidden'
     document.getElementById('tra3').style.visibility='hidden'
  }
 if('<?php echo $datosCargo3["sitrev"]?>'=='In')
  {
     document.getElementById('tit3').style.visibility='hidden'
     document.getElementById('sup3').style.visibility='hidden'
     document.getElementById('tra3').style.visibility='hidden'
  }
 if('<?php echo $datosCargo3["sitrev"]?>'=='Su')
  {
     document.getElementById('tit3').style.visibility='hidden'
     document.getElementById('int3').style.visibility='hidden'
     document.getElementById('tra3').style.visibility='hidden'
  }
 if('<?php echo $datosCargo3["sitrev"]?>'=='Tr')
  {
     document.getElementById('tit3').style.visibility='hidden'
     document.getElementById('int3').style.visibility='hidden'
     document.getElementById('sup3').style.visibility='hidden'
  }
 if('<?php echo $datosCargo3["sitrev"]?>'=='-')
  {
     document.getElementById('tit3').style.visibility='hidden'
     document.getElementById('int3').style.visibility='hidden'
     document.getElementById('sup3').style.visibility='hidden'
     document.getElementById('tra3').style.visibility='hidden'     
  }

</script>
<div style='align:left;position:absolute; left: 100px; top: 1442px; width: 540px; height: 60px;;font-size:12px'><?php print $datosCargo3["obs"]."-". $datosCargo3["licencia"]?></div>
<div style='align:left;position:absolute; left: 105px; top: 1332px; width: 458px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'>
<?php 
    if($datosCargo3["horas"]<>0)
	{print $datosCargo3["CodCargo"]."-".$datosCargo3["espcurr"]."(".$datosCargo3["curso"]."&deg;  ".$datosCargo3["division"]."&deg; )";	 }
?>
</div>


<!-- Cargo 4 -->
<div style='align:left;position:absolute; left: 413px; top: 1517px; width: 657px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php echo nombredependencia($datosCargo4["dependencia"])?></div>
<div style='align:left;position:absolute; left: 300px; top: 1553px; width: 184px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php echo date('d-m-Y',strtotime($datosCargo4["altacargo"]))?></div>
<div style='align:left;position:absolute; left: 94px; top: 1590px; width: 636px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'><?php 
  if($datosCargo4["curso"]==0)
     {print nombredefuncion($datosCargo4["funcion"]);} 
   else 
     {print nombredefuncion($datosCargo4["funcion"])."(".$datosCargo4["horas"]." Hs. Cat)";}?>
</div>
<!-- Situacion de Revista -->
<div id='tit4' style='align:left;position:absolute; left: 1039px; top: 1592px; width: 24px; height: 21px;'>X</div>
<div id='int4' style='align:left;position:absolute; left: 182px; top: 1631px; width: 24px; height: 21px;'>X</div>
<div id='sup4' style='align:left;position:absolute; left: 340px; top: 1630px; width: 24px; height: 21px;'>X</div>
<div id='tra4' style='align:left;position:absolute; left: 760px; top: 1629px; width: 24px; height: 21px;'>X</div>
<script>
 if('<?php echo $datosCargo4["sitrev"]?>'=='Ti')
  {
     document.getElementById('int4').style.visibility='hidden'
     document.getElementById('sup4').style.visibility='hidden'
     document.getElementById('tra4').style.visibility='hidden'
  }
 if('<?php echo $datosCargo4["sitrev"]?>'=='In')
  {
     document.getElementById('tit4').style.visibility='hidden'
     document.getElementById('sup4').style.visibility='hidden'
     document.getElementById('tra4').style.visibility='hidden'
  }
 if('<?php echo $datosCargo4["sitrev"]?>'=='Su')
  {
     document.getElementById('tit4').style.visibility='hidden'
     document.getElementById('int4').style.visibility='hidden'
     document.getElementById('tra4').style.visibility='hidden'
  }
 if('<?php echo $datosCargo4["sitrev"]?>'=='Tr')
  {
     document.getElementById('tit4').style.visibility='hidden'
     document.getElementById('int4').style.visibility='hidden'
     document.getElementById('sup4').style.visibility='hidden'
  }
 if('<?php echo $datosCargo4["sitrev"]?>'=='-')
  {
     document.getElementById('tit4').style.visibility='hidden'
     document.getElementById('int4').style.visibility='hidden'
     document.getElementById('sup4').style.visibility='hidden'
     document.getElementById('tra4').style.visibility='hidden'     
  }

</script>
<div style='align:left;position:absolute; left: 86px; top: 1761px; width: 540px; height: 51px;;font-size:12px'><?php print $datosCargo4["obs"]."-". $datosCargo4["licencia"]?></div>
<div style='align:left;position:absolute; left: 104px; top: 1651px; width: 458px; height: 21px; opacity:100; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;'>
<?php 
    if($datosCargo4["horas"]<>0)
	{print $datosCargo4["CodCargo"]."-".$datosCargo4["espcurr"]."(".$datosCargo4["curso"]."&deg;  ".$datosCargo4["division"]."&deg; )";	 }
?>
</div>
</body>
</html>
