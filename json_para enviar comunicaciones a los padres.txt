{

   lectura:
        [
            {
                id(padre): 1532,
                tipo:"leido" / "notificado"
                fecha: ¨12/12/12 15:15¨
            },
            {
                id(padre): 1532,
                tipo:"leido" / "notificado"
                fecha: ¨12/12/12 15:15¨
            },
        ],

  respuesta:
  {
     alumnos:[
              {nombre:"HermanoA",id:1},
              {nombre:"HermanoB",id:2},
              {nombre:"HermanoC",id:3}
             ],

    comunicados:
          [
            {
              id:0, fecha: "12/12/19", titulo: "Titulo ejemplo", idAlumno:0,
            },
            {
              id:1, fecha: "10/12/19", titulo: "Titulo ejemplo", leido: false,
              resumen: "Cuerpo de la comunicacion Cuerpo de la comunicacion ...",
              cuerpo: "Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion",
            },
            {
              id:2, fecha: "10/12/19", titulo: "Titulo ejemplo leido", leido: true,
              resumen: "Cuerpo de la comunicacion Cuerpo de la comunicacion ...",
              cuerpo: "Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion",
            },
            {
              id:3, fecha: "7/12/19", titulo: "Titulo ejemplo leido", leido: true,
              resumen: "Cuerpo de la comunicacion Cuerpo de la comunicacion ...",
              cuerpo: "Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion",
            },
            {
              id:4, fecha: "10/12/19", titulo: "Titulo ejemplo", leido: false,
              resumen: "Cuerpo de la comunicacion Cuerpo de la comunicacion ...",
              cuerpo: "Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion"
            },
            {
              id:5, fecha: "5/12/19", titulo: "Titulo ejemplo leido", leido: true,
              resumen: "Cuerpo de la comunicacion Cuerpo de la comunicacion ...",
              cuerpo: "Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion Cuerpo de la comunicacion"
            }
         ]

}



/*
json
login 
    ->mail
    ->pass
    <-token
    <-mensaje
    <-dni Familiar
Comunicados
    ->id fam/ dni fam
    ->token
    ->filtros
    <-dni alumno
    <-Destinatario
    <-mes
    <-estado (no leido / Leido / Notificado)
Estado
    ->id Fam / Dni Fam
    ->token
    ->id_comunicado
FullComunicado
    ->idFam/DniFam
    ->token
    ->idComunicado
SetLectura
    ->id Fam / Dni Fam
    ->token
    ->id_comunicado
    <-Resultado
NuevoComunicado
    ->idFam/DniFam
    ->IdAlumno / Dni Alumno
    ->Destinatario
    ->Titulo
    ->Cuerpo
    ->IdComunicado Asociado
LogOut
    ->idFam/DniFam
    ->token
    
*/