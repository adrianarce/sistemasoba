<?php
   session_start();
   include("conexion.php");
?>

<html lang='es'>
<head>
<meta charset="utf-8" lang="es"> 
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
<link rel="stylesheet" 	      href="js/jquery-ui-1.10.3.custom.css" type="text/css" />
<link rel="icon" type="image/png" href="imagenes/escudCTP2.gif" sizes="16x16">
<link rel="stylesheet" href="estiloCTP.css" >

<script>

function buscarDocente()
{
 ndoc=document.getElementById('artDni').value
 fech=document.getElementById('artFecha').value
 if(fech=='') {alert('Indique la fecha para continuar!')}
 else
 {
    window.open("artBuscaDocente.php?dni="+ndoc+"&fecha="+fech, 'opciones')
 }
}
function AgregaArtCargo(cual)
{
  if(cual.checked)
  {
  cars=document.getElementById("cargosenloquesolicita").innerHTML
  cars=cars+cual.id
  document.getElementById("cargosenloquesolicita").innerHTML=cars
  }
  else
  {
  cars=document.getElementById("cargosenloquesolicita").innerHTML
  cars=cars.replace(cual.id,'')
  document.getElementById("cargosenloquesolicita").innerHTML=cars
  
  }
}
function guardar()
{
frm=document.getElementById('articuloFRM')
if(confirm('Realmente desea solicitar este Artículo?'))
  {
    frm.submit()
  }
}
function controlafecha(cual)
{
if( (new Date(cual.value).getTime() <= new Date().getTime()))
    {
      alert('No es posible solicitar un Articulo retroactivo!')
      document.getElementById(cual.id).value=''
      document.getElementById(cual.id).focus()
    }
}
</script>
<body>
<form name='articuloFRM' id='articuloFRM' method='POST' >
<input type='hidden' name='artidAgente' id='artidAgente'>
<table class='Estilo66' align='center'>
<Caption style='font: small-caps 100%/150% serif;color:black;font-size:20px;background-color:gray'> Solicitud de Articulos </caption>
<tr><td>Fecha de Ausencia<input type='date' id='artFecha' name='artFecha' onblur='controlafecha(this)'></td></tr>
<tr><td><input type='text' id='artDni' name='artDni' placeholder='DNI' onblur='buscarDocente()' autocomplete='off'></td></tr>
<tr><td><input type='text' id='artApeynom' name='artApeyNom' placeholder='Apellido y nombre del docente' size='40' autocomplete='off'></td></tr>
<tr><td><div style='position:relative' id='artCargo' name='artCargo'></div></td></tr>
<tr><td><textarea name='cargosenloquesolicita' id='cargosenloquesolicita' style='display:none;'></textarea></td></tr>

<tr><td><textarea id='artRazon' name='artRazon' cols=40 rows=6 placeholder='Motivos...'></textarea></td></tr>
<tr><td align='center'>
     <input type='radio' id='artTipo6VD' name='artTipo' value='6VD' > <label for="artTipo6VD">Sin Descuento de Haberes</label>
     <input type='radio' id='artTipo6VG' name='artTipo' value='6VG' > <label for="artTipo6VG">Con Descuento de Haberes</label>
</td></tr>
<tr><td>Fecha de Solicitud <input type='date' id='artFechaSol' name='artFechaSol' value='<?php echo date("Y-m-d");?>' readonly=true></td></tr>
<tr><td style='background-color:gray'><img src='imagenes/save_32.png' style='float:right;' onclick='guardar()'></td> </tr>
<tr><td id='ArtSolicitados'>Articulos Solicitados por el Agente</td> </tr>

</table>
</form>

<?php
if(isset($_POST['artDni']))
{
$cargos=str_replace('Cargo','/',$_POST['cargosenloquesolicita']);
$cons="insert into AgenteArticulos (FechaAusencia,cargos,Motivos,fechaSolicitud,idAgente,TipoArticulo) values('".$_POST['artFecha']."' ,'".$cargos."','".$_POST['artRazon']."','".$_POST['artFechaSol']."','".$_POST['artidAgente']."','".$_POST['artTipo']."')";
$resalu=mysqli_query($link,$cons) or die("Error al seleccionar seleccionar Listado de Cursos.<hr>".mysqli_error($link)."<hr>".$cons);
}


/*
2	FechaAusencia	date			No	Ninguna		
3	cargos	varchar(60)	utf8_spanish_ci		No	Ninguna		
4	Motivos	varchar(200)	utf8_spanish_ci		No	Ninguna		
5	fechaSolicittud	date			No	Ninguna		
6	idAgente	int(11)			No	Ninguna		
7	Preaviso	int(11)			No	Ninguna		
8	FechaProc	date			No	Ninguna		
9	TipoArticulo	varchar(3)	utf8_spanish_ci		No	Ninguna		
10	FechaDireccion	date			No	Ninguna		
11	firma	varchar(200)	utf8_spanish_ci		No	Ninguna		
12	Obs	tinytext	utf8_spanish_ci		No	Ninguna		
}
*/

?>
<iframe name='opciones' style='position:relative;top:700px;padding-top:40px;border:2px solid black;display:block'></iframe>
</body>
</html>
<!-- 
  Faltas Docentes (Articulos)
  IdDocente
  FechaAusencia
  Cargo 
  Turno
  Hs Catedra
  Cursos
  Turno
  Razones
  FechaSolicitud
  Firma
  -Informe Secretaria
  
  Preaviso 2 dias habiles? [S/N]
  Fecha
  Firma
  -Resolucion
  
  
  6VD - 6VG
  Fecha
  Firma
  Obs  
  
  
  nmea 2000
Garmin echomap 60
-->


