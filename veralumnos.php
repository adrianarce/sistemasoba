<title>Detalles del Alumno</title>
<link rel="stylesheet" href="estiloCTP.css" >


<script>
function cambiasitu(sele)
{
  if(sele.value=='rep')
  { document.getElementById('aniosrepdiv').style.visibility="visible"}
  else
  { document.getElementById('aniosrepdiv').style.visibility="hidden"}
}
function enviar2(frm)
{
   if(window.confirm("¿Desea guardar las modificaciones?"))
   {
     frm.submit();
   }
}
function agregar(frm)
{
  frm.reset();
  frm.dninuevo.readonly=false;
}
function imprimeactualizacion()
{
 var dni=document.getElementById('dninuevo').value
 window.open('alumnosactualizacion.php?dni='+dni,'_blank')
}
</script>

<body onLoad="javascript:carganac();" style="text-align:center;background-color:transparent">
<?php
include('conexion.php');
// ver si el dni del familiar no esta vacio darde alta
$buscado=$_POST["dni"];
if($buscado==""){$buscado=$_GET["dni"];}
$nros = array("0", "9", "8", "7","6","5","4","3","2","1");
if (in_array(substr($buscado,0,1),$nros))
{
   $cons="select * from ctpoba.alumnos where dni='".$buscado."'";
}
else
{
   $cons="select * from ctpoba.alumnos where apeynom like '%".$buscado."%'";
  
}
$resalu=mysqli_query($link,$cons);
$dato=mysqli_fetch_array($resalu);
?>
<form name="f1" method="post" action="grabalumno.php">
<table class="Estilo66" border="1" align=center>
   <caption style="background:#99CCFF;;"> Datos del Alumno 
    <img src="imagenes/save_32.png" height="20px" title='Grabar Modificaciones' class='blue' onclick="javascript:enviar2(f1);" >
    <img src="imagenes/newspaper_32.png"  title="Ver Listado del curso al que asiste" height="20px" class='blue' onClick="javscript:window.open('vercurso.php?idcurso=<?php echo $dato["idcurso"]?>','_self','width=620,height=600,scrollbars=YES');" >
    <img src="imagenes/users_business_32.png"  title="Ver familiares" height="20px" class='blue' onClick="javscript:window.open('familiares.php?idalumno=<?php echo $dato["idalumno"]?>','curso','width=520,height=600,scrollbars=YES');">
    <img src="imagenes/add_32.png"  height="20px" title='Agregar Alumno' class='blue' onclick='javascript:agregar(f1);'>
    <img src="imagenes/print4.png"  height="20px" title='Imprimir hoja de Actualizacion' class='blue' onclick='javascript:imprimeactualizacion();'>
</caption>
<tr><td colspan="0"> 
   <table class='Estilo66'><tr><td> Nro de Documento</td><td>Situacion del Alumno</td><td>Apellido y Nombre</td></tr>
           <tr><td>
   				<input type="hidden" name="dni" value="<?php echo $dato["dni"]?>" maxlength="8" readonly='true' size="9" />
					<input type="text" id='dninuevo' name="dninuevo" value="<?php echo $dato["dni"]?>" maxlength="8" size="9" /></td>
					<td>
                   <select name="sitalu" onchange="cambiasitu(this)">
                      <option value="">Seleccione...</option>
                      <option value="reg"<?php if($dato["situacion"]=='reg'){print " selected ";}?>>Regular</option>
                      <option value="rep"<?php if($dato["situacion"]=='rep'){print " selected ";}?>>Repitente</option>
                      <option value="lib"<?php if($dato["situacion"]=='lib'){print " selected ";}?>>Libre</option>
                      <option value="pas"<?php if($dato["situacion"]=='pas'){print " selected ";}?>>Pase</option>
                      <option value="aba"<?php if($dato["situacion"]=='aba'){print " selected ";}?>>Abandono</option>
						 </select>			
						 <div id="aniosrepdiv" style="position:relative;visibility:<?php if($dato["situacion"]=='rep'){print 'visible';}else{ print 'hidden';}?>; width:120px ;position:absolute;left:270px;top:55px">
						    <select name="crepite">  
                         <option	 value="0">Seleccione...</option>					    
                         <option	 value="1" <?php if($dato["crepite"]=='1'){print " selected ";}?>>1&ordf; vez</option>					    
                         <option	 value="2" <?php if($dato["crepite"]=='2'){print " selected ";}?>>2&ordf; vez</option>					    
                         <option	 value="3" <?php if($dato["crepite"]=='3'){print " selected ";}?>>3&ordf; vez</option>					    
                         <option	 value="4" <?php if($dato["crepite"]=='4'){print " selected ";}?>>4&ordf; vez</option>					    
                         <option	 value="5" <?php if($dato["crepite"]=='5'){print " selected ";}?>>5&ordf; vez</option>					    
                         <option	 value="6" <?php if($dato["crepite"]=='6'){print " selected ";}?>>6&ordf; vez</option>					    
                         <option	 value="7" <?php if($dato["crepite"]=='7'){print " selected ";}?>>7&ordf; vez</option>					  
                         <option	 value="8" <?php if($dato["crepite"]=='8'){print " selected ";}?>>8&ordf; vez</option>					    
                         <option	 value="9" <?php if($dato["crepite"]=='9'){print " selected ";}?>>9&ordf; vez</option>					    
                         <option	 value="10" <?php if($dato["crepite"]=='10'){print " selected ";}?>>10&ordf; vez</option>					    
						    </select></div>		
					</td>
					<td><input type="text" name="apeynom" value="<?php echo $dato["apeynom"]?>" size="40" />
					</tr>	</table>


   </td></td></tr>
       
   <tr>
    <td>Domicilio<br><?php include('datosDom.php');?></td>
    
    </tr>
    <tr>
    <td> <table class='Estilo66'>
       <tr><td>Curso</td><td>Telefono</td></tr>
       <tr><td>
        <select name="idcurso" id="idcurso"> 
            <option value='0'>...</option>
                <?php
                    $sql="select a.idcurso, a.anio,	a.division, a.idciclo, a.idespecialidad, b.nombre as nomespe, c.codigo  from ctpoba.cursos a left join ctpoba.especialidades b on a.idespecialidad=b.idespecialidad inner join ctpoba.ciclos c on a.idciclo=c.idciclo";        
                    $res=mysqli_query($link,$sql) or die ("error al seleccionar cursos!".mysql_error($link));
                    while($curso=mysqli_fetch_array($res)) 
                    {
                    print "<option value='".$curso["idcurso"]."'";
                    if($curso["idcurso"]==$dato["idcurso"])
                    {
                        print " selected ";          	
                        }
                    print " >".$curso["anio"]."&ordm; ".$curso["division"]."&ordf; -".$curso["codigo"]."-".$curso["nomespe"]."</option>\n";
                    }   
                ?>
            </select>
    </td>
    <td><input type="text" name="telefono" size="30"	value="<?php echo $dato["telefono"]?>"	/></td>
   </tr>	 
   <tr><td colspan="4"><table class='Estilo66'>  
   <tr><td>Fecha de Ingreso<br>  <input id="fchin" name="fchin" type="date"  autocomplete="off" value="<?php print date("d/m/Y",strtotime($dato["fchin"]))?>" size="10"/></td>
       <td>Fecha de Egreso<br>  <input id="fnac" name="fchegr" type="date"  autocomplete="off" value="<?php print date("d/m/Y",strtotime($dato["fchegr"]))?>" size="10"/></td>
		 <td>Libro C.B.<br><input type="text" name="libroe" value="<?php echo $dato["libroe"]?>" size="3"/></td>
       <td>Folio C.B.<br><input type="text" name="folioe" value="<?php echo $dato["folioe"]?>" size="3"	/></td>
	    <td>Libro C.O.<br><input type="text" name="librop" value="<?php echo $dato["librop"]?>" size="3"	/></td>
       <td>Folio C.O.<br><input type="text" name="foliop" value="<?php echo $dato["foliop"]?>" size="3"/></td></tr>
		</table></td></tr>
	  <tr><td colspan="2" align="center">Datos de Nacimiento <?php include("datosNac.php");?></td>	
	      
	  <tr>
	    <td align="left" valign="top" colspan="4">Observaciones <textarea name="obs" cols="70" rows="3"><?php echo $dato["obs"]?></textarea></td>  
	  </tr>
  </table>
</FORM> 
  <table class="Estilo66" width="100%">
    <caption style="background:#99CCFF;"> Alumnos encontrado de acuerdo al criterio de busqueda</caption>
	<?php
	while($dato=mysqli_fetch_array($resalu))
	{	  ?>
	  <tr><td style='cursor:pointer;' onClick="javascript:window.open('veralumnos.php?dni=<?php echo $dato["dni"]?>','_self');"> <?php echo $dato["dni"]?></td><td><?php echo $dato["apeynom"]?></td><td>
	<?php }	?>

	</table>
</body>
</html>
