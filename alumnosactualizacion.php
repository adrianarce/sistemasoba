<?php
include('conexion.php');
// ver si el dni del familiar no esta vacio darde alta
$buscado=$_GET["dni"];
$cons="select * from ctpoba.alumnos where dni='".$buscado."'";
$resalu=mysqli_query($link,$cons);
$datosalumno=mysqli_fetch_array($resalu);

function traecurso($idcurso)
{
include('conexion.php');
$cons="select a.anio,a.division,b.nombre as ciclo, c.nombre as especialidad, a.turno from ctpoba.cursos a inner join ciclos b on a.idciclo=b.idciclo left join especialidades c on a.idespecialidad=c.idespecialidad where a.idcurso='".$idcurso."'";
$resalu=mysqli_query($link,$cons);
$dato=mysqli_fetch_array($resalu);
$curso=$dato['anio']."&deg; ".$dato['division']."&deg; </b>Ciclo:<b>".$dato['ciclo']. "</b>";
if(is_null($dato[' especialidad'])==false){$curso.=" Especialidad:<b>Sin especialidad</b>";} else {$curso.=" Especialidad:<b>".$dato['especialidad']. "</b>";}
$curso.=" Turno:<b>";
if($dato['turno']=='M'){$curso.=' Ma&ntilde;ana</b>';}
if($dato['turno']=='T'){$curso.=' Tarde</b>';}
if($dato['turno']=='V'){$curso.=' Vespertino</b>';}

return $curso;

}
function traeciudad($codigo)
{
include('conexion.php');
$cons="select * from lista_ciudades where id='".$codigo."'";
$resalu=mysqli_query($link,$cons);
$dato=mysqli_fetch_array($resalu);
$curso=$dato['opcion'];
return $curso;

}

function traeprovincia($codigo)
{
include('conexion.php');
$cons="select * from lista_estados where id='".$codigo."'";
$resalu=mysqli_query($link,$cons);
$dato=mysqli_fetch_array($resalu);
$curso=$dato['opcion'];
return $curso;

}
function traepais($codigo)
{
include('conexion.php');
$cons="select * from lista_paises where id='".$codigo."'";
$resalu=mysqli_query($link,$cons);
$dato=mysqli_fetch_array($resalu);
$curso=$dato['opcion'];
return $curso;

}
?>
<table>
  <caption>
        <h2>ACTUALIZACION DE DATOS <h2>
        (Para ser completada y firmada por los Sres. Padres/Tutores con Tutorial legal)
 </caption>
 <tr><td>
          CURSO:<b><?php print traecurso($datosalumno['idcurso']);?></b></td></tr>
<tr><td align='center'> DATOS DEL ALUMNO:</td></tr>
<tr><td>NOMBRE Y APELLIDO COMPLETOS:<b><?php print $datosalumno['apeynom']?></b>&nbsp;&nbsp;&nbsp;DNI N°&nbsp;&nbsp;<b><?php print $datosalumno['dni']?></b></td></tr>
<tr><td>
DOMICILIO:<br>Calle: <b><?php print $datosalumno['domcalle']?> </b> n&uacute;mero: <b><?php print $datosalumno['domnro']?></b> Piso: <b><?php print $datosalumno['dompiso']?> </b> Depto:<b> <?php print $datosalumno['domdepto']?></b>&nbsp;&nbsp;TEL:<?php print $datosalumno['telefono']?></td></tr>
<tr><td>
FECHA DE NACIMIENTO:&nbsp;&nbsp;<b><?php print substr($datosalumno['fnac'],6,2).'-'.substr($datosalumno['fnac'],4,2).'-'.substr($datosalumno['fnac'],0,4);?></b>&nbsp;&nbsp;LUGAR DE NACIMIENTO&nbsp;<b><?php print traeciudad($datosalumno['lnac'])?></b>&nbsp;&nbsp;PROVINCIA &nbsp;<b><?php print traeprovincia($datosalumno['provnac'])?></b></td></tr>
<tr><td style='tex-align:center; border: 1px solid black'>
DATOS DEL PADRE, TUTOR O ENCARGADO:
</tr></td>
<tr><td style='tex-align:center; border: 1px solid black'>
<?php 
$cons="select * from ctpoba.familiares where dni='".$buscado."' and relacion='padre'";
$resalu=mysqli_query($link,$cons);
$datospadre=mysqli_fetch_array($resalu);
?>
NOMBRE Y APELLIDO:<b> <?php print $datospadre['apeynom'];?>&nbsp;&nbsp;</b>DNI N° &nbsp;<b> <?php print $datospadre['dnifamiliar'];?></b><br>
DOMICILIO REAL:Calle: <b><?php print $datospadre['domcalle']?> </b> n&uacute;mero: <b><?php print $datospadre['domnro']?></b> Piso: <b><?php print $datospadre['dompiso']?> </b> Depto:<b> <?php print $datospadre['domdepto']?></b>&nbsp;&nbsp;(<b><?php print traeciudad($datospadre['domciudad']);?></b>-<b><?php print traeprovincia($datospadre['domprovincia']);?></b>-<b><?php print traepais($datospadre['dompais']);?></b>)
<br>TELÉFONO FIJO &nbsp;<b><?php print $datospadre['telfijo'];?>&nbsp;&nbsp;</b>CELULAR:<b> <?php print $datospadre['telmovil'];?></b>&nbsp;&nbsp; TEL. LABORAL <b><?php print $datospadre['tellaboral'];?>&nbsp;&nbsp;</b>  
<br> LUGAR DE TRABAJO <b><?php print $datospadre['lugartrabajo'];?>&nbsp;&nbsp;</b><br>
NACIONALIDAD&nbsp;<b><?php print $datospadre['nacionalidad'];?>&nbsp;&nbsp;</b> <br>
<?php 
  if($datospadre['certificado']=='SI')
  {
?>
NECESITA CONSTANCIA ALUMNO REGULAR: SI<br>
Para ser presentado en (nombre de la empresa o institución):<b> <?php print $datospadre['lugartrabajo'];?></b>&nbsp;&nbsp;
<?php }?>
</tr></td>
<tr><td style='text-align:center; border: 1px solid black'>

DATOS DE LA MADRE, TUTORA O ENCARGADA:
<tr/></td>
<tr><td style='tex-align:center; border: 1px solid black'>
<?php 
$cons="select * from ctpoba.familiares where dni='".$buscado."' and relacion='madre'";
$resalu=mysqli_query($link,$cons);
$datospadre=mysqli_fetch_array($resalu);
?>
NOMBRE Y APELLIDO:<b> <?php print $datospadre['apeynom'];?>&nbsp;&nbsp;</b>DNI N° &nbsp;<b> <?php print $datospadre['dnifamiliar'];?></b><br>
DOMICILIO REAL:Calle: <b><?php print $datospadre['domcalle']?> </b> n&uacute;mero: <b><?php print $datospadre['domnro']?></b> Piso: <b><?php print $datospadre['dompiso']?> </b> Depto:<b> <?php print $datospadre['domdepto']?></b>&nbsp;&nbsp;(<b><?php print traeciudad($datospadre['domciudad']);?></b>-<b><?php print traeprovincia($datospadre['domprovincia']);?></b>-<b><?php print traepais($datospadre['dompais']);?></b>)
<br>TELÉFONO FIJO &nbsp;<b><?php print $datospadre['telfijo'];?>&nbsp;&nbsp;</b>CELULAR:<b> <?php print $datospadre['telmovil'];?></b>&nbsp;&nbsp; TEL. LABORAL <b><?php print $datospadre['tellaboral'];?>&nbsp;&nbsp;</b>  
<br> LUGAR DE TRABAJO <b><?php print $datospadre['lugartrabajo'];?>&nbsp;&nbsp;</b><br>
NACIONALIDAD&nbsp;<b><?php print $datospadre['nacionalidad'];?>&nbsp;&nbsp;</b> <br>
<?php 
  if($datospadre['certificado']=='SI')
  {
?>
NECESITA CONSTANCIA ALUMNO REGULAR: SI<br>
Para ser presentado en (nombre de la empresa o institución):<b> <?php print $datospadre['lugartrabajo'];?></b>&nbsp;&nbsp;
<?php }?>
</tr></td>
<tr><td>



SI SU HIJO  TIENE ALGÚN IMPEDIMENTO PARA REALIZAR ED.FÍSICA Ó SI PADECE  ALGUNA ENFERMEDAD  SOLICITAMOS NOS HAGAN  SABER POR MEDIO DE UNA NOTA, ADJUNTANDO CERTIFICADO MÉDICO.  
</td></tr>
<tr><td style='text-align:center; border: 1px solid black'>

PERSONAS AUTORIZADAS A RETIRAR AL ALUMNO
NOMBRE Y APELLIDO
D.N.I.
PARENTESCO
TELEFONO
FIRMA
</td></tr></table>





















…………………………………………………………………….........                             ………………………………………………………..
   FIRMA PADRE, MADRE TUTOR/A O ENCARGADO/A                                        ACLARACIÓN
