<?php session_start();?>
<meta charset="utf-8"> 
<link rel="stylesheet" type="text/css" media="all" href="jscalendar/calendar-green.css" title="win2k-cold-1" />
<link href="estiloCTP.css" rel="stylesheet" type="text/css" />
<script>
function abremenu()
{
   var menu=document.getElementById("opcionesmenu").style.display
   if(menu=='none')
   {
     document.getElementById("opcionesmenu").style.display='flex'
     document.getElementById("tablamenu").style.opacity=1
   }
   else
   {
     document.getElementById("opcionesmenu").style.display='none'
     document.getElementById("tablamenu").style.opacity=0.8
   }
}
function cerrarsession()
{
   window.open('logout.php','_Parent')
}

function cambiaclave()
{

   window.open("cambiaclave.php","contenedor")
   abremenu()
}

function verdocentes()
{
   window.open('buscadocentelogueado.php','opciones')
   abremenu()
}


</script>

  <div style='top:60px;position:absolute;width:100%;border:none;background-color:transparent;'>
     <table id= 'tablamenu' style='float:right;opacity:1;width:250px'>
     <tr><td>
            <img src='imagenes/menu.png' style='float:right;width:48px;cursor:pointer;' title='Ver menu' onclick='abremenu()'>
         </td>
         <td style='display:none;background-color:#00adef;opacity:1' id='opcionesmenu'>
             <table>
                <caption class='button2'>Menú<br><?php print $_SESSION["fullusuario"]?></caption>
                
                <tr><td class='button2' align='center' >Docentes</td></tr>
                <tr><td class='menu' onclick='verdocentes()'>Datos Personales y de Servicio del Docente</td></tr>
                <tr><td class='button2' align='center' >Gesti&oacute;n Usuarios</td></tr>
                <tr><td class='menu' onclick='cambiaclave()'>Cambiar su clave de acceso</td></tr>
                <tr><td class='button2' align='center' onclick='cerrarsession()'>Cerrar la Sessi&oacute;n</td></tr>
            </table> 
         </td></tr>
     </table>
   </div>

