-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 22-08-2019 a las 14:05:14
-- Versión del servidor: 5.7.27-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.21-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ctpoba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `fullname` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `clave` varchar(60) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `rol` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `idagente` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `tokenhora` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `fullname`, `username`, `email`, `clave`, `rol`, `idagente`, `token`, `tokenhora`) VALUES
(1, 'Arce Anibal Adrian', 'adrian', 'solbearce@hotmail.com', '$2y$10$ZVnneg/Y2O4SwkNgaflZduElZ2ksLF1rzZAojGqLgxJ41A4hJv6gC', 'Preceptor', 201, '', NULL),
(2, 'Usuario Administrador', 'admin', '', '$2y$10$gy0rM7ciZRzU3s/Y71J2TujqngwypONAteEkGQMqD/dW52Q5LAqPG', 'Admin', 2, '', NULL),
(3, 'CORONEL JULIO ARGENTINO', 'CORONELJ', NULL, '$2y$10$53TGWHmZnp.LyOZsQxfj..RBb11HpaWxQY8uKysExnZe4w5AihzYa', 'Jefatura', 211, '', NULL),
(4, 'TAPIA ARIEL EDUARDO', 'TAPIAA', NULL, '$2y$10$HToBwuw6CuVUYELp0ULWu.QFFPOUbx3rEQtiuSr078e5NHEgWziNu', 'Admin', 182, '', NULL),
(5, 'GUAYMAS LEONARDO MARTIN', 'GUAYMASL', 'guaymas@leonardo.martin', '$2y$10$gAhxH5NiC45KH42GyOlbju82pM2k5ZtHLPCMAjOQvbLO6V2nAKh/6', 'Familiar', 243, '', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
