<?php
session_start();
$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',PDO::ATTR_PERSISTENT => true);
$conexion = new PDO('mysql:host=localhost;dbname=juntasec;charset=utf8', "user", "Pass.2018", $options);
$conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$conexion->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$conexion->setAttribute(PDO::MYSQL_ATTR_DIRECT_QUERY,false);
//busco el nro de resolucion activa
$sql2 = "select * from resoluciones order by resId desc limit 1";
$con2=$conexion->prepare($sql2);
$con2->execute();
$res=$con2->fetch();
if($_SESSION["usuario"]=="")
{?>
<meta charset="utf-8"> 
<link rel="stylesheet" type="text/css" media="all" href="jscalendar/calendar-green.css" title="win2k-cold-1" />
<script type="text/javascript" src="jscalendar/calendar.js"></script>
<script type="text/javascript" src="jscalendar/lang/calendar-es.js"></script>
<script type="text/javascript" src="jscalendar/calendar-setup.js"></script>
<script type="text/javascript" src="./js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.10.3.custom.js"></script>
<link href="./css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
<link href="estilojunta.css" rel="stylesheet" type="text/css" />
<center style='margin-top:0px; margin-left:0px'><img  style='margin-top:0px; margin-left:0px' src='images/membretes-sup-GOBTDF.png'></center>
<div style='width:380px;margin-left:auto;margin-right:auto;display:inline-block;'>
<form name='loginFRM' id='loginFRM' method='post' action='login.php' target='opciones'>
<table style='color:#FFF;margin-left:auto'>
   <caption>Ingreso al Sistema</caption>
   <tr> <td>Nombre de Usuario</td><td><input type='text' name='usuario'></td></tr>
   <tr> <td>Clave de Usuario</td><td><input type='password' name='clave'></td></tr>
   <tr> <td>Resolución Nro</td><td><input type='text' name='resolucion' value='<?php print $res['resNro'].'/'.substr($res['resAnio'],-2);?>'></td></tr>
   <tr><td colspan=2 align='center'>
    <button type='button' onclick='javascript:this.form.submit();'> Ingresar</button>
    <button type='button' onclick='javascript:window.open("registro.php","_self");'> Registrarse</button>
</table>
</form>
</div>
<?php
}
else
{?>
<script>
function abremenu()
{
   var menu=document.getElementById("opcionesmenu").style.display
   if(menu=='none')
   {
     document.getElementById("opcionesmenu").style.display='flex'
     document.getElementById("tablamenu").style.opacity=1
   }
   else
   {
     document.getElementById("opcionesmenu").style.display='none'
     document.getElementById("tablamenu").style.opacity=0.8
   }
}
function cerrarsession()
{
   window.open('logout.php','opciones')
}
function merituar()
{
   window.open("merituacion.php","contenedor")
   abremenu()
}
function verinscripcion()
{
   window.open("recepcion.php","contenedor")
   abremenu()
}
function incumbencias()
{
   window.open("incumbencias.php","contenedor")
   abremenu()
}
function datosdeldoc()
{
   window.open("datospersonalesDoc.php","contenedor")
   abremenu()
}
function dispo()
{
   window.open("disposiciones.php","contenedor")
   abremenu()
}
function listados()
{
   window.open("listados.php","contenedor")
   abremenu()
}
function controlcasas()
{
   window.open("controltitulos.php","contenedor")
   abremenu()
}
function index()
{
   window.open("detallecarga.php","contenedor")
   abremenu()
}
function renombratablas()
{
   window.open("resoluciones.php","contenedor")
   abremenu()

}
function compararmerituacion()
{
   window.open("comparamerituacion.php","contenedor")
   abremenu()
}
function cambiaclave()
{
   window.open("cambiaclave.php","contenedor")
   abremenu()
}

function tipoCursos()
{
   window.open("tipocursos.php","contenedor")
   abremenu()
  
}
function declaCursos()
{
   window.open("declacursos.php","contenedor")
   abremenu()
}
function informes()
{
   window.open("informes.php","contenedor")
   abremenu()
}
</script>

<center style='margin-top:0px; margin-left:0px'><img  style='margin-top:0px; margin-left:0px;width:auto' src='images/membretes-sup-GOBTDF.png'>
  <div style='top:15px;position:absolute;width:100%;border:none;background-color:transparent;'>
     <table id= 'tablamenu' style='float:right;opacity:1;width:300px'>
     <tr><td>
            <img src='images/menu.png' style='float:right;width:48px;cursor:pointer;' title='Ver menu' onclick='abremenu()'>
         </td>
         <td style='display:none;background-color:#00adef;opacity:1' id='opcionesmenu'>
             <table>
                <caption class='button2'>Menú<br><?php print $_SESSION["fullusuario"]?><hr>Res. Nro:<?php print $_SESSION["resolucion"]?></caption>
                <tr><td class='button2' align='center' >Proceso Inscripci&oacute;n</td></tr>
                <tr><td class='menu' onclick='verinscripcion()'>Ver Inscripci&oacute;n</td></tr>
                <tr><td class='menu' onclick='merituar()'>Merituar</td></tr>
                <tr><td class='menu' onclick='incumbencias()'>Cargar Incumbencias</td></tr>
                <tr><td class='menu' onclick='datosdeldoc()'>Ver Datos Personales</td></tr>
                <tr><td class='menu' onclick='listados()'>Listados</td></tr>
                <tr><td class='menu' onclick='dispo()'>Disposiciones</td></tr>
                <tr><td class='button2' align='center' >Administraci&oacute;n</td></tr>
                <tr><td class='menu' onclick='tipoCursos()'>Tipos de Cursos</td></tr>
                <tr><td class='menu' onclick='declaCursos()'>Declaraci&oacute;n de Cursos</td></tr>
                <tr><td class='menu' onclick='index()'>Informe de Carga</td></tr>
                <tr><td class='menu' onclick='controlcasas()'>Control de Casas de Estudios</td></tr>
                <tr><td class='menu' onclick='controltitulos()'>Control de T&iacute;tulos</td></tr>
                <tr><td class='menu' onclick='compararmerituacion()'>Comparar Merituación</td></tr>
                <tr><td class='menu' onclick='renombratablas()'>Definición de Resoluciones</td></tr>
                <tr><td class='menu' onclick='informes()'>Informes y Controles</td></tr>
                <tr><td class='button2' align='center' >Gesti&oacute;n Usuarios</td></tr>
                <tr><td class='menu' onclick='cambiaclave()'>Cambiar su clave de acceso</td></tr>
                <tr><td class='button2' align='center' onclick='cerrarsession()'>Cerrar la Sessi&oacute;n</td></tr>
            </table> 
         </td></tr>
     </table>
   </div>
</center>
<?php }?>
