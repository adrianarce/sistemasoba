<?php
session_start();
include("conexion2.php");
header ('Content-type: application/pdf; charset=utf-8');
require('fpdf.php');
class PDF extends FPDF
{
   //Cabecera de página
   function Header()
   {
      $this->Image('images/sello junta trans.png',5,3,28);
      $this->SetFont('Helvetica','B',14);
      $this->Cell(0,7,'JUNTA DE CLASIFICACION Y DISCIPLINA DE NIVEL SECUNDARIO',0,1,'C');
      $this->SetFont('Helvetica','BI',10); 
      $listadotit='Listado '.$_GET["tipo"].' de aspirantes a interinatos y suplencias';
      $this->Cell(0,7,$listadotit,0,1,'C');
      $this->Cell(0,7,'Ciclo lectivo 2018',0,1,'C');
      $this->Line(0,30,300,30);
   }
   //Pie de página
    function Footer()
    {
    $this->SetY(-20);
    $this->SetFont('Helvetica','I',8);
    $y=$this->Gety();
    $this->Line(0,$y,220,$y);
    $this->Image('Firmas/vocal4.png',5,330,25);
    $this->Image('Firmas/vocal2.png',45,330,25);
    $this->Image('Firmas/presidente.png',100,330,25);
    $this->Image('Firmas/vocal3.png',150,330,25);
    $this->Image('Firmas/vocal1.png',175,330,25);
    $this->Cell(0,8,'Las Islas Malvinas, Georgias y Sandwich del Sur, son y serán Argentinas',0,1,'C');
    $this->Cell(0,8,'Pag. '.$this->PageNo(),0,1,'R');
    }
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AddPage('p','legal');
$pdf->SetFont('Helvetica','',8);


try
  {
      $sql = "select * from ".$_SESSION["resolucion"]."inscripcion a inner join ".$_SESSION["resolucion"]."espacios b on a.cod_espacio=b.numero inner join niveles c on b.idnivel=c.idNivel inner join ".$_SESSION["resolucion"]."docentes d on a.comprobante=d.comprobante  inner join docentes e on e.doc_codigo=d.doc_codigo where (a.cod_espacio BETWEEN 600 and 624 or a.cod_espacio between 626 and 699)   and ISNULL(caracter)=0 and estado='INSCRIPTO' and caracter<>'999' order by cod_espacio,caracter , puntaje DESC";
      $con=$conexion->prepare($sql);
      $con->execute(); 
      $niveles=$con->fetchall();
      $cod="";
      $linea=0;
      foreach($niveles as $n)
      {         if($n['Des_Nivel']<>$niv)
         {
//            imprimo encabezado del espacios
             $pdf->SetFont('Helvetica','BI',12);
             $des=$n['Des_Nivel'] ;            
             $pdf->Cell(0,7,$des,1,1,'Ć'); 
             $pdf->SetFont('Helvetica','',8);
             $niv=$n['Des_Nivel'];
         }
         if($n['cod_espacio']<>$cod)
         {
//            imprimo encabezado del espacios
             $pdf->SetFont('Helvetica','BI',12);
             $des=$n['cod_espacio'].'-'.$n['descripcion'] ;            
             $pdf->Cell(0,7,$des,1,1,'Ć'); 
             $pdf->SetFont('Helvetica','',8);
             $cod=$n['cod_espacio'];
             
             $indice++; $indice++; 
         }
         $linea++;
         if($linea%2==0){$pdf->SetFillColor(255, 255, 255);} else {$pdf->SetFillColor(238, 238, 238);}
         $des1=$n['cod_espacio'].'-'.$n['descripcion'];      
         $pdf->Cell(20,5,$n['numdoc'],0,0,'C',true);
         $pdf->Cell(80,5,strtoupper($n['apellidos']).", ".ucwords(strtolower(ltrim(rtrim($n['nombres'])))),0,0,'L',true);
         $ciudad="";
         if(substr($n[3],0,1)=='1'){$rio='Río';}else{$rio='---';}
         if(substr($n[3],1,1)=='1'){$tol='Tol';}else{$tol='---';}
         if(substr($n[3],2,1)=='1'){$ush='Ush';}else{$ush='---';}
         $pdf->Cell(30,5,$rio."|".$tol."|".$ush,0,0,'C',true);
         $pdf->Cell(10,5,$n['caracter'],0,0,'C',true);
         if($n["puntaje"]==0){$pdf->Cell(30,5,'-------------',0,1,'C',true);}
         else{$pdf->Cell(30,5,money_format('%= (#10.4n', $n['puntaje']),0,1,'C',true);}
         $indice++;
      }
  }
catch (PDOException $e) 
{
    echo 'Error al recuperar los niveles a listar!: ' . $e->getMessage();
    var_dump($con->debugDumpParams());
}



//Aquí escribimos lo que deseamos mostrar...
$pdf->Output('Listado600.pdf','D');




?>
