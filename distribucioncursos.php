<html lang='es'>
<head>
<meta charset="utf-8" lang="es"> 
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
<link rel="stylesheet" 	      href="js/jquery-ui-1.10.3.custom.css" type="text/css" />

<link rel="stylesheet" href="estiloCTP.css" >
<style type="text/css">
.enjoy-css {
  display: inline-block;
  -webkit-box-sizing: content-box;
  -moz-box-sizing: content-box;
  box-sizing: content-box;
  cursor: pointer;
  padding: 10px 20px;
  border: 1px solid #018dc4;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  font: normal 16px/normal "Palatino Linotype", "Book Antiqua", Palatino, serif;
  color: rgba(255,255,255,0.9);
  -o-text-overflow: clip;
  text-overflow: clip;
  background: rgba(252,171,10,1);
  -webkit-box-shadow: 2px 2px 2px 0 rgba(0,0,0,0.37) ;
  box-shadow: 2px 2px 2px 0 rgba(0,0,0,0.37) ;
  text-shadow: -1px -1px 0 rgba(15,73,168,0.66) ;
  -webkit-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
  -moz-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
  -o-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
  transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
}
</style>
<?php
session_start();
include('conexion.php');
if($_REQUEST['newalumno']<>"")
{
  $sql="update ctpoba.alumnos set idcurso=".$_REQUEST['newcurso']." where dni=".$_REQUEST["newalumno"];
  $res=mysqli_query($link,$sql) or die ("Error al actualizar el curso del alumno!.<br>".mysqli_error($link)."<hr>".$sql);
}
?>
</head>

<script type="text/javascript" >
function Abrir(curso)
{
   window.open('distribucioncursos.php?curso='+curso,"_Self")
}

function eliminar(alumno, curso)
{
  window.open('listadoEliminar.php?alumno='+alumno+'&curso='+curso,'opciones2')
}
function Agregar()
{
  alumno=document.getElementById('dnialumno').value
  curso=<?php print $_GET['curso'];?>
  
  window.open('listadoAgregar.php?alumno='+alumno+'&curso='+curso,'opciones2')
}

function Asistencia(curso)
{
   fecha=document.getElementById('fechaTrab').value
   window.open('alumnosInasistencias.php?curso='+curso+'&fecha='+fecha,'','location=0,height=570,width=550,scrollbars=yes,status=0')
}

function verAlumno(dni)
{
window.open("veralumnos.php?dni="+dni, "opciones", )
}
function abrecurricula(fecha)
{
  var curso='<?php print $_GET['curso']?>'
  window.open('alumnoscurricula.php?fecha='+fecha+'&curso='+curso,'opciones2')
}
function abrecargamaterias()
{
 var curso='<?php print $_GET['curso']?>'
  window.open('cursoasignaturas.php?curso='+curso,'','location=0,height=570,width=1050,scrollbars=yes,status=0')
}
function cerrar()
{
  window.open('logout.php','Self')
}
</script>
<body>
<select name='otroCurso' id='otroCurso' class='enjoy-css'  onchange='Abrir(this.value)'>
<option></option>
<?php
$cons="select * from ctpoba.cursos";
$resalu=mysqli_query($link,$cons) or die("Error al seleccionar seleccionar Listado de Cursos.<hr>".mysqli_error($link)."<hr>".$cons);
// 
 while($dato=mysqli_fetch_array($resalu))
{
     print "<option value=".$dato["idcurso"];
     if($_GET["curso"]==$dato["idcurso"]){ print " selected ";}
     print ">".$dato["anio"]." &deg; ".$dato["division"]." &deg;</option>";
}
?>
</select>
<div id='Alumnos'  style='position:relative;left:0px;border:none;width:100%;height:600px;'>Alumnos
   <input type='text' id='dnialumno' name='dnialumno' placeholder='DNI Alumno'><button><img src='imagenes/user_add_32.png' height='24px' onclick='Agregar()'></button> <hr>
   <table class='estilo66'><tr><td>Opc</td><td>Dni</td><td> Apellido y nombre</td></tr>
   <?php
   if(isset($_GET['curso'])){
    $cons="select * from ctpoba.alumnosxcurso a inner join ctpoba.alumnos b on dnialumno=dni where a.idcurso=".$_GET['curso']." order by apeynom";
    $resalu=mysqli_query($link,$cons) or die("Error al seleccionar agentes.<hr>".mysqli_error($link)."<hr>".$cons);
    $x=0;
    while($dato=mysqli_fetch_array($resalu))
    {
     $x++;
     print "<tr><td>";
     print "<img src='imagenes/user_close_32.png' height='24px' title='Quitar al alumno de este curso' style='cursor:pointer;' onclick='eliminar(".$dato["dni"].",".$_GET["curso"].")'>";
     print "<img src='imagenes/user_info_32.png' height='24px' title='Ver la informacion del alumno' style='cursor:pointer;' onclick='verAlumno(".$dato["dni"].")'>";
     print "</td><td>".$x.")".$dato['dni'].'</td><td>'.$dato['apeynom'].'</td></tr>';
    }
    }
   ?>
   </table>
   
</div>
<iframe name='opciones2' ></iframe>
</body>
</html>
