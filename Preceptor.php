<?php session_start();?>
<html>
<head>
<meta charset="utf-8" lang="es"> 
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
<link rel="stylesheet" 	      href="js/jquery-ui-1.10.3.custom.css" type="text/css" />

<link rel="stylesheet" href="estiloCTP.css" >
<style type="text/css">
.enjoy-css {
  display: inline-block;
  -webkit-box-sizing: content-box;
  -moz-box-sizing: content-box;
  box-sizing: content-box;
  cursor: pointer;
  padding: 10px 20px;
  border: 1px solid #018dc4;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  font: normal 16px/normal "Palatino Linotype", "Book Antiqua", Palatino, serif;
  color: rgba(255,255,255,0.9);
  -o-text-overflow: clip;
  text-overflow: clip;
  background: rgba(252,171,10,1);
  -webkit-box-shadow: 2px 2px 2px 0 rgba(0,0,0,0.37) ;
  box-shadow: 2px 2px 2px 0 rgba(0,0,0,0.37) ;
  text-shadow: -1px -1px 0 rgba(15,73,168,0.66) ;
  -webkit-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
  -moz-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
  -o-transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
  transition: all 300ms cubic-bezier(0.42, 0, 0.58, 1);
}
</style>	
<script>
function Abrir(curso,tipo)
{
   window.open('Preceptor.php?curso='+curso+'&tipocurso='+tipo,"_Self")
}

function eliminar(alumno, curso)
{
  window.open('listadoEliminar.php?alumno='+alumno+'&curso='+curso,'opciones')
}
function Agregar()
{
  alumno=document.getElementById('dnialumno').value
  tipocurso=document.getElementById('tipocurso').value
  curso='<?php print $_GET['curso'];?>'
  
  window.open('listadoAgregar.php?alumno='+alumno+'&curso='+curso+'&tipocurso='+tipocurso,'opciones')
}

function Asistencia(curso)
{
   fecha=document.getElementById('fechaTrab').value
   window.open('alumnosInasistencias.php?curso='+curso+'&fecha='+fecha,'','location=0,height=570,width=550,scrollbars=yes,status=0')
}
function parte(curso)
{
   fecha=document.getElementById('fechaTrab').value
   window.open('alumnosParteDiario.php?curso='+curso+'&fecha='+fecha,'','location=0,height=570,width=550,scrollbars=yes,status=0')
}
function verAlumno(dni)
{
window.open("veralumnos.php?dni="+dni, "", "location=0,height=570,width=800,scrollbars=yes,status=0")
}
function abrecurricula(fecha)
{
  var curso='<?php print $_GET['curso']?>'
  window.open('alumnoscurricula.php?fecha='+fecha+'&curso='+curso,'opciones')
}
function abrecargamaterias()
{
 var curso='<?php print $_GET['curso']?>'
  window.open('cursoasignaturas.php?curso='+curso,'','location=0,height=570,width=1050,scrollbars=yes,status=0')
}
function cerrar()
{
  window.open('logout.php','Self')
}

function agregafaltadocente(cual)
{
  fecha=document.getElementById('fechaTrab').value
  window.open('registrafaltadocente.php?idhoraxcargo='+cual+'&fecha='+fecha,'opciones')
}
function cerrarFaltasDoc()
{
 document.getElementById('FaltaDocente').style.display='none'
}
function cerrarFaltasDocDet()
{
 document.getElementById('FaltaDocDetalle').style.display='none'
}
function GrabarFaltasDoc()
{
horacargo=document.getElementById('FaltasDocCargo').value
fecha=document.getElementById('FaltasDocFecha').value
window.open("grabafaltadocente.php?idhoraxcargo="+horacargo+"&fecha="+fecha, 'opciones')
}
function faltasprogramadas()
{
 document.getElementById("FaltaDocDetalle").style.display="block";
}
function infAsistencia(curso)
{
  window.open('InfFaltasAlumnos.php?curso='+curso,'','location=0,height=570,width=1050,scrollbars=yes,status=0')
}
function Comunicados(curso)
{
  window.open('comunicados.php?curso='+curso,'','location=0,height=670,width=950,scrollbars=yes,status=0')
}
</script>
</head>



<body style="text-align:center;background-image:URL('imagenes/fondo7.jpg');background-size:cover;background-attachment:fixed" onload="abrecurricula(document.getElementById('fechaTrab').value)">
<table border=0 width='100%' cellpadding="0" cellspacing="0">
  <tr> 
	  <td width='15%'><img src="imagenes/escudCTP2.gif" height="80px" /></td>
	  <td width='70%' align='center'><h3> Colegio T&eacute;cnico Provincial <br />"Olga B de Arko"</h3></td>
	  <td width='15%'><img src="imagenes/escudotdf.png" height="80px" alt=""></td>
  </tr>
</table>	
<div style='position:absolute;width:99%;background-color:transparent;border:none;'>
<?php  
session_start();
include("conexion.php");
$cons="select * from ctpoba.cursos where idpreceptor=".$_SESSION['idagente'];
$resalu=mysqli_query($link,$cons) or die("Error al seleccionar listado de Cursos del Preceptor.<hr>".mysqli_error($link)."<hr>".$cons);
while($dato=mysqli_fetch_array($resalu))
{
  print "<input type='button' class='login' style='height:30px;cursor:pointer;' value='Curso: ".$dato["anio"]." Divisi&oacute;n: ".$dato["division"]."' onclick='Abrir(". $dato["idcurso"].",\"".$dato["tipocurso"]."\")'>";
}
?>
<select name='otroCurso' id='otroCurso' class='login'  style='height:30px;cursor:pointer;' onchange='Abrir(this.value)'>
<option></option>
<?php
$cons="select * from ctpoba.cursos order by anio, division";
$resalu=mysqli_query($link,$cons) or die("Error al seleccionar seleccionar Listado de Cursos.<hr>".mysqli_error($link)."<hr>".$cons);
// 
 while($dato=mysqli_fetch_array($resalu))
{
  if($dato['idpreceptor']<>$_SESSION['idagente'])
  {
     print "<option value=".$dato["idcurso"];
     if($_GET["curso"]==$dato["idcurso"]){ print " selected ";}
     print ">".$dato["anio"]." &deg; ".$dato["division"]." &deg;</option>";
  }
}
?>
</select>

<input type='date' class='login' style='height:30px' id='fechaTrab' name='fechaTrab' value='<?php print date('Y-m-d')?>' onchange='abrecurricula(this.value)'>
<input type='hidden' value='<?php print $_GET['tipocurso']?>' name='tipocurso' id='tipocurso'>

<button class='login' style='float:right;cursor:pointer;height:30px' onclick='cerrar()' title='Cerrar la Session!'><img src='imagenes/potter.png' style='height:28px;vertical-align:middle;'><?php print $_SESSION['fullusuario']?></button>
</div>

<div id='Alumnos'  style='position:absolute;left:0px;top:150px;border:2px solid black;width:50%;height:400px; overflow:scroll'>Alumnos
   <input type='text' id='dnialumno' name='dnialumno' placeholder='DNI Alumno'><button><img src='imagenes/user_add_32.png' style='height:24px;vertical-align:middle;' onclick='Agregar()'></button> <hr>
   <table class='estilo66'><tr><td>Opc</td><td>Dni</td><td> Apellido y nombre</td></tr>
   <?php
   if(isset($_GET['curso'])){
    $cons="select * from ctpoba.alumnosxcurso a inner join ctpoba.alumnos b on dnialumno=dni where a.idcurso=".$_GET['curso']." order by apeynom";
    $resalu=mysqli_query($link,$cons) or die("Error al seleccionar agentes.<hr>".mysqli_error($link)."<hr>".$cons);
    $x=0;
    while($dato=mysqli_fetch_array($resalu))
    {
     $x++;
     print "<tr><td>";
     print "<img src='imagenes/user_close_32.png' height='24px' title='Quitar al alumno de este curso' style='cursor:pointer;' onclick='eliminar(".$dato["dni"].",".$_GET["curso"].")'>";
     print "<img src='imagenes/user_info_32.png' height='24px' title='Ver la informacion del alumno' style='cursor:pointer;' onclick='verAlumno(".$dato["dni"].")'>";
     print "</td><td>".$x.")".$dato['dni'].'</td><td>'.$dato['apeynom'].'</td></tr>';
    }
    }
   ?>
   </table>
   
</div>

<div id='Materias' style='position:absolute;background-color:black;left:50%;top:150px;border:2px solid blue ;width:49%;height:400px'>Espacios Curriculares
</div>

<div class='divBotones' style='position:absolute;top:560px;width:49%;height:40px;background-color:transparent;border:none;'>
<input type='button' class='login' style='height:30px' value='Registrar Asistencia' onclick='Asistencia(<?php print $_GET["curso"];?>)'>
<input type='button' class='login' style='height:30px' value='Informe de Asistencia' onclick='infAsistencia(<?php print $_GET["curso"];?>)'>
<input type='button' class='login' style='height:30px' value='Notificaciones a los Padres' onclick='Comunicados(<?php print $_GET["curso"];?>)'>
</div>

<div class='divBotones' style='position:absolute;top:560px;;width:49%;height:40px;background-color:transparent;border:none;left:50%'>
<input type='button' class='login' style='height:30px' value='Generar Parte' onclick='parte(<?php print $_GET["curso"];?>)'>
</div>

<div id='FaltaDocente' class='divBotones' style='position:absolute;border:2px solid white;left:400px;top:250px;width:350px;height:auto;background-color:black;display:none;margin-left:auto;margin-right:auto;opacity:1'> </div>


<div id='FaltaDocDetalle' class='divBotones' style='position:absolute;border:2px solid white;left:400px;top:250px;width:750px;height:auto;background-color:black;display:none;margin-left:auto;margin-right:auto;opacity:1'> </div>

<iframe name='opciones' style='position:relative;top:700px;padding-top:40px;border:2px solid black;display:block'></iframe>

</body>
</html>
