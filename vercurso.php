<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title>Alumnos del Curso</title>

<STYLE TYPE="text/css">
  font9{
background-color: #99ccff;
color:#999999 ;
font-family: arial, verdana, ms sans serif;
font-size: 8pt
}

input {
BACKGROUND-COLOR: #99CCFF;
COLOR: BLACK;
FONT-FAMILY: ARIAL, VERDANA, MS SANS SERIF;
FONT-SIZE: 8PT
}
</style>
<script>
	//cargo los alumnos a un vector para que sea mas sencillo el agregar a los alumnos del curso
var alumnosvec=new Array();
<?php 

include('conexion.php');
$alucons="select * from ctpoba.alumnos";
$alures=mysqli_query($link,$alucons) or die("Error al cargar el listado de alumnos".mysql_error($link));
$x=0;
while($fila=mysqli_fetch_array($alures))
{
print "alumnosvec[$x]=new Array('".$fila["dni"]."','".str_replace("'"," ",$fila["apeynom"])."');\n";
$x++;
	}
?>
function agregaralumnoalcurso(elform)
{
 window.open("grabaalumnoxcurso.php?idcurso="+document.agregalu.idcurso.value+"&dni="+document.agregalu.opcionesalu.options[document.agregalu.opcionesalu.selectedIndex].value,"_blank")
}
function alumnosaltextarea()
{
//primero borro las opciones del select multiple
 document.getElementById("opcionesalu").length=1
 document.getElementById("opcionesalu").options[0].value=""
 document.getElementById("opcionesalu").options[0].text=""
 var indice=0
 for(var i=0; i< alumnosvec.length; i=i+1)
	{
	  if(alumnosvec[i][1].substring(0,document.getElementById("inalumno").value.length).toUpperCase()==document.getElementById("inalumno").value.toUpperCase())
     {

     	var elOptNew = document.createElement('option');
          elOptNew.text = alumnosvec[i][1];
          elOptNew.value = alumnosvec[i][0];
         var elSel = document.getElementById('opcionesalu');
         try {
             elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
             }
          catch(ex) {
              elSel.add(elOptNew); // IE only
               }

    }	  
	}

}
function certifica(dni)
{
window.open("inf_alumnoregular.php?dni="+dni,"certificado");
	}
function cargaalumnos()
{
	document.getElementById("agregaalumnos").style.visibility='visible';
}

</script>
</head>

<body onload=" alumnosaltextarea()">
<?php

//$cons="select * from ctpoba.alumnos a inner join ctpoba.cursos b on a.idcurso=b.idcurso  where a.idcurso=".$_GET["idcurso"]." order by apeynom";

//$cons="select * from ctpoba.alumnosxcurso a inner join ctpoba.cursos b on a.idcurso=b.idcurso inner join ctpoba.alumnos c on a.dnialumno=c.dni  where a.idcurso=".$_GET["idcurso"]." order by apeynom";
$cons="select * from ctpoba.cursos a left join ctpoba.alumnos b on a.idcurso=b.idcurso where a.idcurso=".$_GET["idcurso"]." order by apeynom";
$resalu=mysqli_query($link,$cons) or die("error el seleccinar informacion del curso".mysql_error($link));
$curso=mysqli_fetch_array($resalu);
$resalu=mysqli_query($link,$cons);
?>
<div id="agregaalumnos" style="border: 3px coral solid; position:absolute; width:500px; height:300px;background:#00aae8;visibility:hidden;overflow:visible;left:500px;">
<div title="Cerrar" style="border: 3px coral solid; position:absolute; width:30px; height:30px; top: -7px; left: 480px; font-size:15px;background:#00aae8; padding-left:11px; padding-top:12px"><img src="imagenes/close_16.png" style="cursor:pointer;" onclick="javascript:document.getElementById('agregaalumnos').style.visibility='hidden';"></div>
<form name="agregalu">
  <table border=1 >
    <caption>Asignacion de Alumnos por curso</caption>
    <tr> <td> Dni o Nombre del Alumno</td></tr>  
    <tr> <td> <input type="text" id='inalumno' ><input type="hidden" name="idcurso" value="<?php print $_GET["idcurso"]?>"><br>
      <select name="opcionesalu" id="opcionesalu" multiple="true" size="10" ondblclick="javascript:agregaralumnoalcurso(agregalu);"></select>
    </td></tr>
    <tr><td align="center"> <input type="button" value="Agregar Alumno"></td></tr>  
  </table>
</form>

</div>
  <table class="font9" border="1" align="center">
    <tr>
      <td style="border:0px solid ;" align="center"><img src="imagenes/blue_arrow_left_32.png" height="48px" style="cursor:pointer" onclick="javascript:history.back(1);">    </td>
      <td style="border:0px solid ;" align="center" ><?php echo $curso["anio"]?>&deg; a&ntilde;o <?php echo $curso["division"]?>&deg; division</h2> </td>
      <td  style="border:0px solid ;" align="center"> <img src="imagenes/page_text_add_32.png" alt="Imagen" title="Agregar Alumnos al curso/grupo" onclick="javascript:cargaalumnos();" ></td>
    </tr>
	<?php
	while($dato=mysqli_fetch_array($resalu))
	{	  ?>
	  <tr>
	 <td> <img src="imagenes/user_warning_32.png" style='cursor:pointer;' height="24px" title="Modificar Informacion del alumno" onclick="javascript:window.open('veralumnos.php?dni=<?php echo $dato["dni"]?>','_self');">
	      <img src="imagenes/user_close_32.png"style='cursor:pointer;'   height="24px" title="Quitar alumno del curso" onclick="javascript: window.open('grabaalumnoxcurso.php?idcurso=<?php print $_GET["idcurso"]?>&dni=<?php print $dato["dni"]?>','_blank');"></td>
	 <td><?php echo $dato["dni"]?></td><td><?php echo $dato["apeynom"]?></td>
     <td><img src="imagenes/newspaper_32.png" title="Emitir Constancia de alumno regular para este alumno" style="cursor:pointer" onclick="javascript:certifica(<?php echo $dato["dni"]?>);"></td>	
	<?php 
	 print "</tr>";}	?>
	</table>
</body>
</html>


