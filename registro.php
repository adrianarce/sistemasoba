<html>
<head>
<title>Sistema de la Junta de Clasificación y Disciplina - Sede Ushuaia</title>

<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">

<meta charset="utf-8"> 
<link rel="stylesheet" type="text/css" media="all" href="jscalendar/calendar-green.css" title="win2k-cold-1" />
<script type="text/javascript" src="jscalendar/calendar.js"></script>
<script type="text/javascript" src="jscalendar/lang/calendar-es.js"></script>
<script type="text/javascript" src="jscalendar/calendar-setup.js"></script>
<script type="text/javascript" src="./js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.10.3.custom.js"></script>
<link href="./css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
<link href="estilojunta.css" rel="stylesheet" type="text/css" />

</head>
<?php
session_start();
if(isset($_POST["nomyap"]))
{
  try{
	$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',PDO::ATTR_PERSISTENT => true);
$conexion = new PDO('mysql:host=localhost;dbname=juntasec;charset=utf8', "user", "Pass.2018", $options);
$conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$conexion->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$conexion->setAttribute(PDO::MYSQL_ATTR_DIRECT_QUERY,false);

	  $sql = "select * from juntasec.user where username=:username";
	  $con=$conexion->prepare($sql);
	  $con->bindparam(':username',$_POST["usuario"]);
	  $con->execute(); 
	  $espacios=$con->fetchall();
	  if(count($espacios)<>0)
	  {
	  ?>
	  <script>
	  alert("Ya existe el usuario.!")
	  </script>
	 
      <?php
         }
         else
         {
            $creado=date("Y-m-d h:i:s");
            print $creado;
            $clave=password_hash($_REQUEST["clave1"], PASSWORD_BCRYPT);
            $sql = "insert into juntasec.user (fullname,username,email,password,created_at,clave,ciudad,rol) values(:nombre,:usuario,:email,:password,:creado,:clave,:ciudad,:rol)";
            $con=$conexion->prepare($sql);
            $con->bindparam(':nombre',$_POST["nomyap"]);
            $con->bindparam(':usuario',$_POST["usuario"]);
            $con->bindparam(':password',$_POST["clave1"]);
            $con->bindparam(':clave',$clave);
            $con->bindparam(':email',$_POST["email"]);
            $con->bindparam(':creado',$creado);
            $con->bindparam(':ciudad',$_POST["ciudad"]);
            $con->bindparam(':clave',$clave);
            $con->bindparam(':rol',$_POST['rol']);
            $con->execute(); 
            $_SESSION["usuario"]=$_POST["usuario"];
            $_SESSION["fullusuario"]=$_POST["nomyap"];
            header("location:index.php");
         }
      }
  catch (PDOException $e) 
    {
      print 'Falló la carga del usuario: ' . $e->getMessage();
    } 
}
?>
<script>
function registro()
{
  var usuario=document.getElementById("usuario").value
  var clave1=document.getElementById("clave1").value
  var clave2=document.getElementById("clave2").value
  var nomyap=document.getElementById("nomyap").value
  var email=document.getElementById("email").value
  if(clave1 != clave2)
  {
    alert("Las claves no coinciden!")
    return false
  }
  if(clave1=='')
  {
    alert("Las claves no no pueden estar vacías!")
    return false
  }
  if(nomyap=='' || email=='' || usuario=='')
  {
    alert("Debe completar todos los datos para continuar")
    return false
  }
  document.getElementById("loginFRM").submit()
}
function inicio()
{
window.open('index.php','_self')
}
</script>

<body style='background-image:URL("imagenes/fondo7.jpg");;background-size:cover;background-attachment:fixed'>
<iframe style='display:none' name='opciones'></iframe>
<table border=0 width='100%' cellpadding="0" cellspacing="0">
  <tr>
	  <td align='center' style='color:#FFF'><h3> Colegio T&eacute;cnico Provincial <br />"Olga B de Arko"</h3></td>
  </tr>
 </tr>
</table>	

<div style='width:380px;margin-left:auto;margin-right:auto;display:block;'>
<form name='loginFRM' id='loginFRM' method='post'>
<table style='color:#FFF;width:400px' align='center'>
   <caption>Registro de Usuarios<hr></caption>
   <tr><td rowspan=7><img src="imagenes/escudCTP2.gif" height="80px"></td><td>Nombre de Usuario</td><td><input type='text'         id='usuario' name='usuario'></td></tr>
   <tr> <td>Clave de Usuario</td><td><input type='password'      id='clave1' name='clave1'></td></tr>
   <tr> <td>Repita la clave</td><td><input type='password'       id='clave2' name='clave2'></td></tr>
   <tr> <td>Nombre Completo</td><td><input type='text'           id='nomyap' name='nomyap' size=20></td></tr>
   <tr> <td>Correo Electr&oacute;nico</td><td><input type='text' id='email'  name='email'></td></tr>
   <tr> <td>Rol</td><td><select name='rol'>
                <option value=''>Seleccione</option>
                <option value='Docente'>Docente</option>
                <option value='Administrativo'>Administrativo</option>
                <option value='Informatico'>Informatico</option>
                <option value='Padre/Alumno'>Informatico</option>
            </select>
    </td></tr>
   
   <tr><td colspan=2 align='center'>
        <button type='button2' onclick='javascript:registro();'> Registrarse</button>
        <button type='button2' onclick='javascript:inicio();'> Pantalla Inicial</button>
   </td></tr>
</table>
</form>
</div>
</body>
</html>
