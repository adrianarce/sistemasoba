<?php session_start();?>
<meta charset="utf-8"> 
<link rel="stylesheet" type="text/css" media="all" href="jscalendar/calendar-green.css" title="win2k-cold-1" />
<link href="estiloCTP.css" rel="stylesheet" type="text/css" />
<script>
function abremenu()
{
   var menu=document.getElementById("opcionesmenu").style.display
   if(menu=='none')
   {
     document.getElementById("opcionesmenu").style.display='flex'
     document.getElementById("tablamenu").style.opacity=1
   }
   else
   {
     document.getElementById("opcionesmenu").style.display='none'
     document.getElementById("tablamenu").style.opacity=0.8
   }
}
function cambiaclave()
{

   window.open("cambiaclave.php","contenedor")
   abremenu()
}

function cerrarsession()
{
   window.open('logout.php','_Parent')
}
function veralumnos()
{
   window.open("alumnos.php","contenedor")
   abremenu()
}

function vercursos()
{
   window.open("cursos.php","contenedor")
   abremenu()
}
function verdistribucion()
{
   window.open("distribucioncursos.php","contenedor")
   abremenu()
}
function verdocentes()
{
   window.open("docentes.php","contenedor")
   abremenu()
}
function cursoasignaturas()
{
   window.open("cursoasignaturas.php","contenedor")
   abremenu()
}
function informes()
{
   window.open("alumnos.php","contenedor")
   abremenu()
}
function usuarios()
{
   window.open("usuarios.php","contenedor")
   abremenu()
}
function verasig()
{
   window.open("asignaturas.php","contenedor")
   abremenu()
}
function listadosAluFal()
{
   window.open("InfFaltasAlumnos.php","contenedor")
   abremenu()
}
</script>

  <div style='top:15px;position:absolute;width:100%;border:none;background-color:transparent;'>
     <table id= 'tablamenu' style='float:right;opacity:1;width:250px'>
     <tr><td>
            <img src='imagenes/menu.png' style='float:right;width:48px;cursor:pointer;' title='Ver menu' onclick='abremenu()'>
         </td>
         <td style='display:none;background-color:#00adef;opacity:1' id='opcionesmenu'>
             <table  class='Estilo66' style='width:100%'>
                <caption class='button2'>Menú<br><?php print $_SESSION["fullusuario"]?></caption>
                <tr><td class='button2' align='center' >Alumnos</td></tr>
                <tr><td class='menuant' onclick='veralumnos()'>Datos Personales</td></tr>
                <tr><td class='menuant' onclick='vercursos()'>Definicion de Cursos</td></tr>     
                <tr><td class='menuant' onclick='verasig()'>Definicion de Asignaturas</td></tr>
                <tr><td class='menuant' onclick='verdistribucion()'>Asignacion de Cursos</td></tr>
                <tr><td class='menuant' onclick='listadosAluFal()'>Listados Inasistencias Alumnos</td></tr>
                
                <tr><td class='button2' align='center' >Docentes</td></tr>
                <tr><td class='menuant' onclick='verdocentes()'>Datos Personales del Docente</td></tr>
                <tr><td class='menuant' onclick='cursoasignaturas()'>Asignar Cargos y horarios al Doc.</td></tr>
                <tr><td class='button2' align='center' >Gesti&oacute;n Usuarios</td></tr>
                <tr><td class='menuant' onclick='cambiaclave()'>Cambiar su clave de acceso</td></tr>
                <tr><td class='menuant' onclick='usuarios()'>Definicion de Usuarios</td></tr>
                <tr><td class='button2' align='center' onclick='cerrarsession()'>Cerrar la Sessi&oacute;n</td></tr>
            </table> 
         </td></tr>
     </table>
   </div>

