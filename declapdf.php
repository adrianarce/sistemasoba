<?php
session_start();
include('conexion.php');
// ---------------------------------------- Funciones locales -----------------------------//
function traeciudad($ciu, $prov)
{
include('conexion.php');
$ciudadsql="select * from ctpoba.lista_ciudades where id=".$ciu." and relacion=".$prov;
$resciudad=mysqli_query($link,$ciudadsql) or die(mysqli_error($link));
$datociudad=mysqli_fetch_array($resciudad) or die (mysqli_error($link)); 
return $datociudad["opcion"];
}
function nombredefuncion($funci)
{
 include('conexion.php');
   $cons4="select * from ctpoba.funciones where idFuncion='".$funci."'";
   $resfuncion=mysqli_query($link,$cons4) or die(mysqli_error($link).$cons4);
   $datosfuncion=mysqli_fetch_array($resfuncion);
   return $datosfuncion["Descripcion"];
}
function nombredependencia($funci)
{
include('conexion.php');
   $cons4="select * from ctpoba.dependencias where cod_dep='".$funci."'";
   $resfuncion=mysqli_query($link,$cons4) or die(mysqli_error($link).$cons4);
   $datosfuncion=mysqli_fetch_array($resfuncion);
   return $datosfuncion["nom_dep"];
}

//-------------------------------------- Fin funciones locales --------------------------------//

$cons3="select * from ctpoba.agentes where ndoc=".$_GET["doc"];
$resagente=mysqli_query($link,$cons3) or die(mysqli_error($link));
$datosAgente=mysqli_fetch_array($resagente);

$cons3="select * from ctpoba.cargosxagente where dni=".$_GET["doc"]." order by ordenimpres";
$resagente=mysqli_query($link,$cons3) or die(mysqli_error($link));
$cantCargos=$resagente->num_rows;


header('Content-type: application/pdf; charset=utf-8');
include('conexion.php');

require('fpdf.php');
class PDF extends FPDF
{
   //Cabecera de página
   function Header()
   {
     //$this->Image('imagenes/Declajur1.jpg',0,20,210,0);
   }
   //Pie de página
    function Footer()
    {
    $this->SetY(-20);
    $this->SetFont('Helvetica','I',8);
    $y=$this->Gety();
    $this->Line(0,$y,220,$y);
    $this->Cell(0,8,'Pag. '.$this->PageNo(),0,1,'R');
    }
}

$pdf = new FPDF();
$pdf->AddPage('P','legal');
$pdf->SetFont('Arial','B',10);

$pdf->Image('imagenes/declaracion-frente.jpg',0,20,210,0);
$pdf->Cell(40,36,$cantCargos,0,1,'L');//Separador Superior
$pdf->Cell(55,7,'',0,0,'L');//Separador Izquierda
$pdf->Cell(40,7,$datosAgente['apeynom'],0,1,'L');
$pdf->Cell(55,7,'',0,0,'L');//Separador Izquierda
$pdf->Cell(40,7,$datosAgente['fnac'],0,0,'L');
$pdf->Cell(55,7,'',0,0,'L');//Separador Izquierda
$pdf->Cell(40,7,$datosAgente['ndoc'],0,1,'L');

$pdf->Cell(35,6,'',0,0,'L');//Separador Izquierda
$domicilio=$datosAgente['domcalle'].' Nro:'.$datosAgente['domnro'];
if($datosAgente['dompiso']<>''){$domicilio.=' Piso '.$datosAgente['dompiso'];}
if($datosAgente['domdepto']<>''){$domicilio.=' Depto '.$datosAgente['domdepto'];}
$pdf->Cell(100,7,$domicilio,0,0,'L');
$ciudaddom=traeciudad($datosAgente["domciudad"],$datosAgente["domprovincia"]);
$pdf->Cell(20,7,'',0,0,'L');//Separador Izquierda
$pdf->Cell(40,7,$ciudaddom,0,1,'L');
$pdf->Cell(20,17,'',0,1,'L');//Separador Izquierda
$idcargos='';
for($x=0;$x<4;$x++)
{
// --------------------------------------------------- Primer Cargo del Agente ------------------------------------------------------------//
$datosCargo1=mysqli_fetch_array($resagente);
$idcargos.=$datosCargo1["idcargo"].',';
//$pdf->Cell(20,3,'',1,1,'L');//Separador Izquierda
$depe=nombredependencia($datosCargo1["dependencia"]);
$pdf->Cell(70,6,'',0,0,'L');//Separador Izquierda
$pdf->Cell(40,6,$depe,0,1,'L');

$pdf->Cell(53,6,'',0,0,'L');//Separador Izquierda
$fchalta1=date('d-m-Y',strtotime($datosCargo1["altacargo"]));
$pdf->Cell(40,6,$fchalta1,0,1,'L');

if($datosCargo1["curso"]==0)
    {$funcion1=nombredefuncion($datosCargo1["funcion"]);} 
else 
    {$funcion1=nombredefuncion($datosCargo1["funcion"])."(".$datosCargo1["horas"]." Hs. Cat)";}
     
$pdf->Cell(10,6,'',0,0,'L');//Separador Izquierda

$pdf->Cell(120,6,$funcion1,0,0,'L');

// situacion de revista
if($datosCargo1['sitrev']=='Ti')
   {
    $pdf->Cell(55,7,'',0,0,'L');//Separador Izquierda
    $pdf->Cell(40,7,'X',0,1,'L');
    $pdf->Cell(24,7,'',0,1,'L');//Separador Izquierda
   }
else
   {
      if($datosCargo1['sitrev']=='In')
      {
          $pdf->Cell(135,7,'',0,1,'L');//Separador Izquierda
          $pdf->Cell(24,7,'',0,0,'L');//Separador Izquierda
          $pdf->Cell(40,7,'X',0,1,'L');
      }
      else
      {
        if($datosCargo1['sitrev']=='Su')
        {
            $pdf->Cell(135,7,'',0,1,'L');//Separador Izquierda
            $pdf->Cell(54,7,'',0,0,'L');//Separador Izquierda
            $pdf->Cell(40,7,'X',0,1,'L');
        }
        else
        {
            $pdf->Cell(135,7,'',0,1,'L');//Separador Izquierda
            $pdf->Cell(132,7,'',0,0,'L');//Separador Izquierda
            $pdf->Cell(40,7,'X',0,1,'L');
        }
      }
   }
   if($datosCargo1["curso"]<>0)
   {
   $asig=$datosCargo1["CodCargo"]."-".$datosCargo1["espcurr"]."(".$datosCargo1["curso"]."° ".$datosCargo1["division"]."°)";
   }
   else
   {
    $asig='';
   }
   $pdf->Cell(10,4,'',0,0,'L');//Separador Izquierda
   $pdf->Cell(40,4,$asig,0,1,'L');
   $pdf->SetFont('Arial','B',8);
   if($datosCargo1['licencia']<>''){ $info='Lic: '.$datosCargo1['licencia'];}else{$info='';}
   $pdf->Cell(30,15,'',0,1,'L');//Separador Izquierda
   $pdf->Cell(3,15,'',0,0,'L');//Separador Izquierda
   $pdf->Cell(40,4,$info,0,1,'L');
   if($datosCargo1['obs']<>''){ $info='Obs: '.$datosCargo1['obs'];}else{$info='';}
   $pdf->Cell(3,15,'',0,0,'L');//Separador Izquierda
   $pdf->Cell(40,4,$info,0,1,'L');
   $pdf->SetFont('Arial','B',10);

$pdf->Cell(10,8,'',0,1,'L');//Separador de cargos
// --------------------------------------------------------------- Fin primeros cinco cargos ---------------------------------------------------------//   
}
// agrego una hoja para imprimir el 5 cargo y los horarios

$pdf->AddPage('P','legal');
$pdf->SetFont('Arial','B',10);

$pdf->Image('imagenes/declajur3.jpg',03,0,210,0);
$datosCargo1=mysqli_fetch_array($resagente);
$idcargos.=$datosCargo1["idcargo"].',';
$pdf->Cell(20,10,'',0,1,'L');//Separador Izquierda
$depe=nombredependencia($datosCargo1["dependencia"]);
$pdf->Cell(70,6,'',0,0,'L');//Separador Izquierda
$pdf->Cell(40,6,$depe,0,1,'L');

$pdf->Cell(53,6,'',0,0,'L');//Separador Izquierda
$fchalta1=date('d-m-Y',strtotime($datosCargo1["altacargo"]));
$pdf->Cell(40,8,$fchalta1,0,1,'L');

if($datosCargo1["curso"]==0)
    {$funcion1=nombredefuncion($datosCargo1["funcion"]);} 
else 
    {$funcion1=nombredefuncion($datosCargo1["funcion"])."(".$datosCargo1["horas"]." Hs. Cat)";}
     
$pdf->Cell(10,8,'',0,0,'L');//Separador Izquierda

$pdf->Cell(120,8,$funcion1,0,0,'L');

// situacion de revista
if($datosCargo1['sitrev']=='Ti')
   {
    $pdf->Cell(60,6,'',0,0,'L');//Separador Izquierda
    $pdf->Cell(40,6,'X',0,1,'L');

    $pdf->Cell(24,6,'',0,1,'L');//Separador Izquierda
   }
else
   {
      if($datosCargo1['sitrev']=='In')
      {
          $pdf->Cell(135,7,'',0,1,'L');//Separador Izquierda
          $pdf->Cell(24,7,'',0,0,'L');//Separador Izquierda
          $pdf->Cell(40,7,'X',0,1,'L');
      }
      else
      {
        if($datosCargo1['sitrev']=='Su')
        {
            $pdf->Cell(135,7,'',0,1,'L');//Separador Izquierda
            $pdf->Cell(54,7,'',0,0,'L');//Separador Izquierda
            $pdf->Cell(40,7,'X',0,1,'L');
        }
        else
        {
            $pdf->Cell(135,7,'',0,1,'L');//Separador Izquierda
            $pdf->Cell(132,7,'',0,0,'L');//Separador Izquierda
            $pdf->Cell(40,7,'X',0,1,'L');
        }
      }
   }
   if($datosCargo1["curso"]<>0)
   {
   $asig=$datosCargo1["CodCargo"]."-".$datosCargo1["espcurr"]."(".$datosCargo1["curso"]."° ".$datosCargo1["division"]."°)";
   }
   else
   {
    $asig='';
   }
   $pdf->Cell(10,4,'',0,0,'L');//Separador Izquierda   
   $pdf->Cell(40,4,$asig,0,1,'L');
   $pdf->SetFont('Arial','B',8);
   if($datosCargo1['licencia']<>''){ $info='Lic: '.$datosCargo1['licencia'];}else{$info='';}
   $pdf->Cell(30,19,'',0,1,'L');//Separador Izquierda
   $pdf->Cell(3,15,'',0,0,'L');//Separador Izquierda
   $pdf->Cell(40,4,$info,0,1,'L');
   if($datosCargo1['obs']<>''){ $info='Obs: '.$datosCargo1['obs'];}else{$info='';}
   $pdf->Cell(3,15,'',0,0,'L');//Separador Izquierda
   $pdf->Cell(40,4,$info,0,1,'L');
   $pdf->SetFont('Arial','B',10);



/////// Imprimo los horarios
if(substr($idcargos,-1)==','){$idcargos=substr($idcargos,0,-1);}
$pdf->Cell(60,8,$idcargos,0,1,'L');//Separador de cargos

$cons3="SELECT * FROM cargosxagente a inner join horarioxcargo b on a.idcargo=b.idcargo where a.idcargo in(".$idcargos.") ORDER BY ordenimpres,a.idcargo,dia, desde";

$resagente=mysqli_query($link,$cons3) or die(mysqli_error($link).$cons3);

$datosCargo2=mysqli_fetch_array($resagente);

$pdf->SetFont('Arial','B',7);
$pdf->Cell(15,9,'',0,1,'L');//Separador Izquierda
$depe=nombredependencia($datosCargo2["dependencia"]);
$asig=$datosCargo2["CodCargo"]."-".$datosCargo2["espcurr"]."(".$datosCargo2["curso"]."° ".$datosCargo2["division"]."°)";

$depe1=substr($depe,0,28);
$depe2=substr($depe,28);
//$pdf->MultiCell(37,7,$depe.$asig,0,1,'C');
/*
$pdf->Cell(37,7,$depe1,0,1,'C');
$pdf->Cell(37,7,$depe2,0,1,'C');

$asig1=substr($asig,0,28);
$asig2=substr($asig,28);
$pdf->Cell(37,7,$asig1,0,1,'C');
$pdf->Cell(37,6,$asig2,0,1,'C');*/

$cargo=$datosCargo2["idcargo"];
$lu='';$Ma='';$Mi='';$Ju='';$Vi='';$Sa=''
while($cargo==$datosCargo2["idcargo"])
{
   $datosCargo2=mysqli_fetch_array($resagente);
   if($datosCargo2=='Lu'){$lu=$lu ."De ".$datosCargo2['desde']." a ".$datosCargo2['hasta']."\n";}
}

$pdf->SetFont('Arial','B',7);
$depe=nombredependencia($datosCargo2["dependencia"]);
$asig=$datosCargo2["CodCargo"]."-".$datosCargo2["espcurr"]."(".$datosCargo2["curso"]."° ".$datosCargo2["division"]."°)";

$depe1=substr($depe,0,28);
$depe2=substr($depe,28);
$pdf->Cell(37,6,$depe1,0,1,'C');
$pdf->Cell(37,6,$depe2,0,1,'C');

$asig1=substr($asig,0,28);
$asig2=substr($asig,28);
$pdf->Cell(37,6,$asig1,0,1,'C');
$pdf->Cell(37,7,$asig2,0,1,'C');


$cargo=$datosCargo2["idcargo"];
while($cargo==$datosCargo2["idcargo"])
{
   $datosCargo2=mysqli_fetch_array($resagente);
}

$pdf->SetFont('Arial','B',7);
$depe=nombredependencia($datosCargo2["dependencia"]);
$asig=$datosCargo2["CodCargo"]."-".$datosCargo2["espcurr"]."(".$datosCargo2["curso"]."° ".$datosCargo2["division"]."°)";

$depe1=substr($depe,0,28);
$depe2=substr($depe,28);
$pdf->Cell(37,6,$depe1,0,1,'C');
$pdf->Cell(37,7,$depe2,0,1,'C');

$asig1=substr($asig,0,28);
$asig2=substr($asig,28);
$pdf->Cell(37,7,$asig1,0,1,'C');
$pdf->Cell(37,7,$asig2,0,1,'C');



$cargo=$datosCargo2["idcargo"];
while($cargo==$datosCargo2["idcargo"])
{
   $datosCargo2=mysqli_fetch_array($resagente);
}

$pdf->SetFont('Arial','B',7);
$depe=nombredependencia($datosCargo2["dependencia"]);
$asig=$datosCargo2["CodCargo"]."-".$datosCargo2["espcurr"]."(".$datosCargo2["curso"]."° ".$datosCargo2["division"]."°)";

$depe1=substr($depe,0,28);
$depe2=substr($depe,28);
$pdf->Cell(37,6,$depe1,0,1,'C');
$pdf->Cell(37,6,$depe2,0,1,'C');

$asig1=substr($asig,0,28);
$asig2=substr($asig,28);
$pdf->Cell(37,7,$asig1,0,1,'C');
$pdf->Cell(37,7,$asig2,0,1,'C');


$cargo=$datosCargo2["idcargo"];
while($cargo==$datosCargo2["idcargo"])
{
   $datosCargo2=mysqli_fetch_array($resagente);
}

$pdf->SetFont('Arial','B',7);
$depe=nombredependencia($datosCargo2["dependencia"]);
$asig=$datosCargo2["CodCargo"]."-".$datosCargo2["espcurr"]."(".$datosCargo2["curso"]."° ".$datosCargo2["division"]."°)";

$depe1=substr($depe,0,28);
$depe2=substr($depe,28);
$pdf->Cell(37,6,$depe1,0,1,'C');
$pdf->Cell(37,6,$depe2,0,1,'C');

$asig1=substr($asig,0,28);
$asig2=substr($asig,28);
$pdf->Cell(37,7,$asig1,0,1,'C');
$pdf->Cell(37,7,$asig2,0,1,'C');

$nombre='borrame.pdf';
$pdf->Output();
?>



