<?php header('Content-type: text/html; charset=utf-8'); ?>
<html>
<head>
<title>Ingreso al Sistema</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<?php 
include("conexion.php");
?>
<script language="javascript1.5">
function limpiar(frm)
{
document.frmCur.espcurcom.value=""
document.frmCur.curso.value=""
document.frmCur.idasignatura.value=""
document.frmCur.idciclo.value=""
document.frmCur.especialidad.value=""
document.frmCur.inslegal.value=""

}
function enviar(frm)
{
  if(confirm("Desea grabar la Asignatura?"))
  {
     frm.submit()
  }
}
function elimina(id,espacio,curso,especialidad)
{
  if(confirm("Desea eliminar la asignatura \n"+espacio +" de "+curso+" a�o "+ especialidad +" ? "))
  {
     window.open("asignaturas.php?idasig="+id+"&elimina=S","_self"); 
  }
}
</script>
<body style="text-align:center;background-color:transparent;">
<?php

if($_GET["elimina"]=="S")
{
$cons="delete from ctpoba.asignaturas where idasignatura='".$_GET["idasig"]."'";
$resalu=mysqli_query($link,$cons);
}
if($_POST["anio"]<>"")
{
   if($_POST["idasig"]=="")
      {
          $cons="insert into ctpoba.asignaturas(espcurcom, anio, idciclo, especialidad, hs, inslegal) values('".$_POST["espcurcom"]."','".$_POST["anio"]."','".$_POST["idciclo"]."','".$_POST["idespecialidad"]."','".$_POST["hs"]."','".$_POST["inslegal"]."')";
          $resalu=mysqli_query($link,$cons) or die (mysqli_error($link)."<hr>".$cons);
      }
   else
      {
          $cons="update ctpoba.asignaturas set espcurcom='".$_POST["espcurcom"]."', anio='".$_POST["anio"]."' ,especialidad ='".$_POST["idespecialidad"]."', idciclo='".$_POST["idciclo"]."', hs='".$_POST["hs"]."',inslegal='".$_POST["inslegal"]."' where idasignatura='".$_POST["idasig"]."'";
          $resalu=mysqli_query($link,$cons) or die (mysqli_error($link));
	  }
}
$cons="select * from ctpoba.asignaturas where idasignatura='".$_GET["idasig"]."'";
$resalu=mysqli_query($link,$cons);
$curso=mysqli_fetch_array($resalu)


?>
<form method="post" name="frmAsig"  action="asignaturas.php">
<input type="hidden" name="idasig" value="<?php print $curso["idasignatura"]?>" />
<table align="center" style='text-align:left;'>
  <caption> Definicion de Asignaturas</caption>
  <tr><td>Espacio Curricular</td><td><input type="text" size="40" maxlength="100"  name="espcurcom" value="<?php print $curso["espcurcom"]?>" /></td>  </tr>
  <tr><td>A&ntilde;o</td><td><input type="text" size="4" maxlength="4"  name="anio" value="<?php print $curso["anio"]?>" /></td></tr>
  <tr><td>Especialidad</td><td> 
 <select name="idespecialidad">
 <option value="0">...</option>

  <?php
     $sql="Select * from ctpoba.especialidades";
     $res=mysqli_query($link,$sql) or die("Error al generar listado de especialidades<hr>".mysqli_error());
     while($fila=mysqli_fetch_array($res))
     {
       print  "<option value=".$fila["idespecialidad"];
       if($fila["idespecialidad"]==$curso["especialidad"]){print " selected ";}       
       print ">".$fila["nombre"]."</option>"; 	
     }
     
  ?>  
 </select><?php print $curso["especialidad"];?>
      </td></tr>

  <tr><td>Ciclo</td><td>     <select name="idciclo">
			<option>...</option>
		<?php 
		  $cons="Select * from ctpoba.ciclos";
		  $res=mysqli_query($link,$cons) or die("Error al obtener listado de Ciclos<hr>".mysqli_error());
		  while($fila=mysqli_fetch_array($res)) 
		  {
		  	 print "<option value='".$fila["idciclo"]."' ";
		  	 if($curso["idciclo"]==$fila["idciclo"]){print " selected ";}
		  	 print ">".$fila["nombre"]."</option>";
		  	}
		?>      
      </select>
  </td></tr>
  <tr><td>Horas</td><td><input type="text" name="hs"  value="<?php print $curso["hs"]?>" size="5" /></td></tr>
  <tr><td>Resoluci&oacute;n</td><td><input type="text" name="inslegal"  value="<?php print $curso["inslegal"]?>" size="50" /></td></tr>
  <tr><td colspan="2">
  <img src="imagenes/folder_add_32.png" onClick="javascript:limpiar(frmAsig);" style="cursor:pointer"/>
  <img src="imagenes/save_32.png" width="32" height="32" onClick="javascript:enviar(frmAsig);" style="cursor:pointer"/>
  </td>
</table>
</form>

<?php
$cons="select a.idasignatura, a.hs, a.espcurcom, a.idciclo, a.anio, a.especialidad, b.nombre as nomespe, c.codigo  from ctpoba.asignaturas a left join ctpoba.especialidades b on a.especialidad=b.idespecialidad inner join ctpoba.ciclos c on a.idciclo=c.idciclo order by idciclo,especialidad,anio, espcurcom";
// aca vemos si se esta grabando o eliminando


$resalu=mysqli_query($link,$cons)or die(mysqli_error());
?>
  <table class="font9" align="center">
    <caption>Listado de Asignaturas Definidas</caption>
    <tr><td>Opc</td><td>Curso</td> <td>Espacio Curricular</td><td>Hs.</td><td>Ciclo</td><td>Especialidad</td></tr>
	<?php
	$x=0;
	while($dato=mysqli_fetch_array($resalu))
	{$x=$x + 1;	  ?>
	  <tr <?php if($x%2==0){print "style='background-color:#e6e6e6'";} else {print "style='background-color:#A0A0A0'";} ?>><td>
	   <img src="imagenes/pencil_32.png" height="16" title="Modificar" style="cursor:pointer" onClick="javascript:window.open('asignaturas.php?idasig=<?php print $dato["idasignatura"]?>','_self');">
	   <img src="imagenes/close_16.png" width="16" height="16" style="cursor:pointer" title="Eliminar" onClick="javascript:elimina('<?php print $dato["idasignatura"]?>','<?php print $dato["espcurcom"]?>','<?php print $dato["anio"]?>','<?php print $dato["especialidad"]?>');">
	   
	   </td>
	   <td><?php echo $dato["anio"]?></td> <td style='text-align:left;' ><?php echo $dato["espcurcom"]?></td><td><?php echo $dato["hs"]?></td><td><?php echo $dato["codigo"]?></td><td><?php echo $dato["nomespe"]?></td></tr>
	<?php 
	
	 }	?>

</table>
</body>

</html>
