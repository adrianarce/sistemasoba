<link href="estiloCTP.css" rel="stylesheet" type="text/css" />



<script>
function abremenu()
{
   var menu=document.getElementById("opcionesmenu").style.display
   if(menu=='none')
   {
     document.getElementById("opcionesmenu").style.display='flex'
     document.getElementById("tablamenu").style.opacity=1
   }
   else
   {
     document.getElementById("opcionesmenu").style.display='none'
     document.getElementById("tablamenu").style.opacity=0.8
   }
}
function cerrarsession()
{
   window.open('logout.php','_Parent')
}
function veralumnos()
{
   window.open("alumnos.php","contenido")
   abremenu()
}

function vercursos()
{
   window.open("cursos.php","contenido")
   abremenu()
}
function verdistribucion()
{
   window.open("distribucioncursos.php","contenido")
   abremenu()
}
function verdocentes()
{
   window.open("docentes.php","contenido")
   abremenu()
}
function cursoasignaturas()
{
   window.open("cursoasignaturas.php","contenido")
   abremenu()
}
function informes()
{
   window.open("alumnos.php","contenido")
   abremenu()
}
function usuarios()
{
   window.open("usuarios.php","contenido")
   abremenu()
}
function verasig()
{
   window.open("asignaturas.php","contenido")
   abremenu()
}
function verciclos()
{
   window.open("ciclos.php","contenido")
   abremenu()
}
function verespecialidades()
{
   window.open("especialidades.php","contenido")
   abremenu()
}
function articulos()
{
   window.open("agentesarticulo.php","contenido")
   abremenu()
}
function cambiaclave()
{
   window.open("cambiaclave.php","contenido")
   abremenu()
}
function articulospend()
{
   window.open("artPendientes.php","contenido")
   abremenu()
   
}
</script>

<!-- 
<div id='menu' style="position:absolute;left:-70px; top:80px;text-align:center; width:950px">
<nav class="menuCSS3">
<ul id="nav">
	<li class="top"><a href="index3.php" class="top_link"><span>Inicio</span></a></li>
    <li class="top"><a href="#nogo2" id="products" class="top_link"><span class="down">Alumnos</span></a>
		<ul class="sub">
			<li><a href="alumnos.php" onclick='abremenu()' target='opciones'>Alumnos</a></li>
			<li><a href="distribucioncursos.php"  target='opciones'>Distribuci&oacute;n de Alumnos en Cursos</a></li>
			<li><a href="inf_curso.php"  target='opciones'>Listados</a></li>
		</ul>
	</li>
	<li class="top"><a href="#nogo22" id="services" class="top_link"><span class="down">Docentes</span></a>
		<ul class="sub">
			<li><a href="docentes.php" onclick='abremenu()' target='opciones'>Docentes</a></li>
			<li><a href="generaplanilla.php">Planillas de Calificaciones</a></li>
		</ul>
	</li>
	<li class="top"><a href="#nogo27" id="contacts" class="top_link"><span class="down">Informes</span></a>
		<ul class="sub">
			<li><a href="ordendecla.php">Cambiar el Orden de la Declaracion</a></li>
		</ul>
	</li>
	<li class="top"><a href="#nogo53" id="Informes" class="top_link"><span class="down">Configuraci&oacute;n</span></a>
		<ul class="sub">	
			<li><a href="cursos.php" target='opciones'>Cursos</a></li><br>
			<li><a href="asignaturas.php" target='opciones'>Asignaturas</a></li><br>
			<li><a href="especialidades.php" target='opciones'>Especialidades</a></li><br>
			<li><a href="ciclos.php" target='opciones'>Ciclos</a></li><br>
		</ul>
	</li>
	<li class="top"><a  id="usuarios" class="top_link"><span class="down">Usuarios</span></a>
		<ul class="sub">
			<li><a href="cambiaclave.php">Cambiar mi clave</a></li>
			<li><a href="usuarios.php" target='opciones'>Definicion de Usuarios</a></li>
			<li><a href="permisoUsuarios.php">Permisos de Usuario</a></li>
		</ul>
	</li>
	<li class="top"><a href="logout.php" class="top_link"><span>Cerrar Sesi&oacute;n</span></a></li>	
	<li class="top"><a href='#' onclick='abremenu()' class="top_link"><span>MENU </span><img src='imagenes/iconomenu.png' height='24px'></a></li>	
	
</ul>
</nav>
</div>
-->
  <div style='top:60px;position:absolute;width:100%;border:none;background-color:transparent;'>
     <table id= 'tablamenu' style='float:right;opacity:1;width:250px'>
     <tr><td>
            <img src='imagenes/menu.png' style='float:right;width:48px;cursor:pointer;' title='Ver menu' onclick='abremenu()'>
         </td>
         <td style='display:none;background-color:#00adef;opacity:1' id='opcionesmenu'>
             <table class='Estilo66' style='width:100%'>
                <caption class='button2'>Menú<br><?php print $_SESSION["fullusuario"]?></caption>
                <tr><td class='button2' align='center'          >Alumnos</td></tr>
                <tr><td class='menuant' onclick='veralumnos()'     >Datos Personales</td></tr>
                <tr><td class='menuant' onclick='verdistribucion()'>Asignacion de Cursos</td></tr>
                <tr><td class='menuant' onclick='listados()'>  Listados</td></tr>
                
                <tr><td class='button2' align='center'>Docentes</td></tr>
                <tr><td class='menuant' onclick='verdocentes()'        >Datos Personales del Docente</td></tr>
                <tr><td class='menuant' onclick='cursoasignaturas()'   >Asignar Cargos y horarios al Doc.</td></tr>
                <tr><td class='menuant' onclick='articulos()'          >Solicitud de Articulos del Docente</td></tr>
                <tr><td class='menuant' onclick='articulospend()'      >Articulos Pendientes</td></tr>
                

                <tr><td class='button2' align='center'>Configuracion</td></tr>
                <tr><td class='menuant' onclick='vercursos()'      >Definicion de Cursos</td></tr>     
                <tr><td class='menuant' onclick='verasig()'        >Definicion de Asignaturas</td></tr>
                <tr><td class='menuant' onclick='verciclos()'      >Definicion de Ciclos</td></tr>     
                <tr><td class='menuant' onclick='verespecialidades()'        >Definicion de Especialidades</td></tr>
                
                <tr><td class='button2' align='center'              >Gesti&oacute;n Usuarios</td></tr>
                <tr><td class='menuant' onclick='cambiaclave()'     >Cambiar su clave de acceso</td></tr>
                <tr><td class='menuant' onclick='usuarios()'        >Definicion de Usuarios</td></tr>
                <tr><td class='button2' align='center' onclick='cerrarsession()'>Cerrar la Sessi&oacute;n</td></tr>
            </table> 
         </td></tr>
     </table>
   </div>

<!--
   Definir roles
      Depto Alumnos
        Planillas para Mesas de Examen
        Calificaciones de los alumnos
        Listados de Alumnos
        Emision de Certificados Analiticos
        
      Secretaria
        Listado de Docentes
        Planta Funcional
        Informe de Ausencias
        
      Jefatura de Preceptores
        Parte Diario
        Apertura Cierre de cursos
        
      Preceptores
        Control de Asistencia
        Planilla de calificaciones
        informe de Asistencia del Docente
        
      Padres
        Consulta sobre inasistencias y calificaciones del alumnos
        
      Informatica
        Control de 

-->
