  
  <script src="js/jquery-1.3.2.min.js" type="text/javascript" charset="utf-8"></script>
  <script src="js/jquery.ui-1.7.min.js" type="text/javascript" charset="utf-8"></script>
  <script src="js/rb_menu.js" type="text/javascript" charset="utf-8"></script>


  
  <link rel="stylesheet" type="text/css" href="css/rb_menu.css" media="screen,projection" />

<div id="menu1" class="menu clearfix">
  <div class="toggle">menu &#187;</div>  
  <div class="items">  
    <ul>
      <li><a href="">Home</a></li>
      <li><a href="">Facts</a></li>

      <li><a href="">Projects</a></li>
      <li><a href="">Press</a></li>
      <li><a href="">Jobs</a></li>
      <li><a href="">Contacts</a></li>
      <li><a href="">Log in</a></li>
    </ul>

  </div>
</div>

<hr />

triggered using <strong>click</strong>:

<div id="menu2" class="menu clearfix">
  <div class="toggle">menu &#187;</div>  
  <div class="items">  
    <ul>
      <li><a href="">Home</a></li>

      <li><a href="">Facts</a></li>
      <li><a href="">Projects</a></li>
      <li><a href="">Press</a></li>
      <li><a href="">Jobs</a></li>
      <li><a href="">Contacts</a></li>
      <li><a href="">Log in</a></li>

    </ul>
  </div>
</div>

<hr />

a different slide animtion ('easein'):

<div id="menu3" class="menu clearfix">
  <div class="toggle">menu &#187;</div>  
  <div class="items">  
    <ul>
      <li><a href="">Home</a></li>

      <li><a href="">Facts</a></li>
      <li><a href="">Projects</a></li>
      <li><a href="">Press</a></li>
      <li><a href="">Jobs</a></li>
      <li><a href="">Contacts</a></li>
      <li><a href="">Log in</a></li>

    </ul>
  </div>
</div>

<script type="text/javascript" charset="utf-8">
$('#menu1').rb_menu();
$('#menu2').rb_menu({triggerEvent: 'click', hideOnLoad: true, loadHideDelay: 0, autoHide: false});
$('#menu3').rb_menu({transition: 'swing'});
</script>
